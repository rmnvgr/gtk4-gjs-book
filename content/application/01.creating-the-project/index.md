---
title: Creating the Project
summary: Setting up the development environment for a GTK4+GJS app with Flatpak and Meson
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/creating-the-project
weight: 1
slug: creating-the-project
---

This section contains a lot a boilerplate code, but that is necessary in order to have an easy to work with environment!

## Choosing an Application ID

The application ID will identify our application across the platform. It is in the reverse DNS form, and should be composed of a domain we own, and the application name.

Let's say we control the `example.org` domain, and our application name is "File Browser". The application ID is then `org.example.filebrowser`.

{{% info %}}
The GNOME Developer Documentation has a tutorial on [choosing an application ID][GNOME Application ID] if you want to learn more about it.
{{% /info %}}

## Directory Structure

It's time to create a source directory for our application. We name it by the ID of our application:

```sh {linenos=none}
mkdir org.example.filebrowser
```

Inside, we create the directory structure that will contain our project files:

- `src` will contain our JavaScript modules
- `data` will contain our additional application data, such as icons, UI definitions, CSS files

You can use these commands to create these directories in your application directory:

```sh {linenos=none}
cd org.example.filebrowser
mkdir src data
```

## Necessary Files

Now let's create all the basic files required for running our application.

### Main Module

This file is the heart of our application. It must contain a `main()` function that will be run when it starts. Let's create it in `src/main.js`, and print a simple message to the console inside:

```js
export function main(argv) {
	console.log('Hello World!');
}
```

### Resources

The build system will compile our sources and data files into [GResources][Gio.Resource]. We have to create two XML files describing our resources.

`src/org.example.filebrowser.src.gresource.xml`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<gresources>
	<gresource prefix="/org/example/filebrowser/js">
		<file>main.js</file>
	</gresource>
</gresources>
```

`data/org.example.filebrowser.data.gresource.xml`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<gresources>
	<gresource prefix="/org/example/filebrowser">
	</gresource>
</gresources>
```

Don't forget to update these files when we add files to the application!

### Entry Point

This is a special file that will initialize our GJS package and start the application. Create a `src/org.example.filebrowser.js` file and put this inside:

```js
#!@GJS@ -m

import GLib from 'gi://GLib';

// Initialize the package
imports.package.init({
	name: '@PACKAGE_NAME@',
	version: '@PACKAGE_VERSION@',
	prefix: '@PREFIX@',
	libdir: '@LIBDIR@',
});

// Import the main module and run the main function
const loop = new GLib.MainLoop(null, false);
import('resource:///org/example/filebrowser/js/main.js')
	.then((main) => {
		GLib.idle_add(GLib.PRIORITY_DEFAULT_IDLE, () => {
			loop.quit();
			imports.package.run(main);
			return GLib.SOURCE_REMOVE;
		});
	})
	.catch(logError);
loop.run();
```

All the values between `@` will be replaced by the build system when compiling the application.

So what does it do? `package` is a special GJS module containing functions that will initialize our package (`package.init()`) by setting some directories and loading resources, and run our application (`package.run()`).

After initializing the package, we create a loop to asynchronously load our main module from the resources. The loop is then stopped, and our application takes over from there.

### Build System

Now, we have to tell Meson, our build system, what to do with all these files. Let's create all the `meson.build` files with our instructions.

In `meson.build`, we give some information about our project, set the `APP_ID` variable to reuse the application ID elsewhere without having to retype it, import the [`gnome` module][Meson gnome], and load `meson.build` files from the `data` and `src` directories:

```meson
# Define our project
project(
	'filebrowser',
	version: '0.0.1',
	license: ['GPL-3.0-or-later'],
	meson_version: '>= 0.59.0'
)

APP_ID = 'org.example.filebrowser'

# Import the modules
gnome = import('gnome')

# Load instructions from subdirectories
subdir('data')
subdir('src')
```

In `data/meson.build`, we compile the data resources with the [`gnome.compile_resources()` function][Meson gnome.compile_resources()] and install them in our application data directory:

```meson
# Compile the resources
gnome.compile_resources(
	APP_ID + '.data',
	APP_ID + '.data.gresource.xml',
	gresource_bundle: true,
	install: true,
	install_dir: get_option('datadir') / APP_ID
)
```

In `src/meson.build`, we also compile the source resources, and we configure our entry point file and install it as an executable in the binaries directory:

```meson
# Configure the entry point
configure_file(
	input : APP_ID + '.js',
	output : APP_ID,
	configuration: {
		'GJS': find_program('gjs').full_path(),
		'PACKAGE_NAME': APP_ID,
		'PACKAGE_VERSION': meson.project_version(),
		'PREFIX': get_option('prefix'),
		'LIBDIR': get_option('prefix') / get_option('libdir')
	},
	install_dir: get_option('bindir'),
	install_mode: 'rwxr-xr-x'
)

# Compile the resources
app_resource = gnome.compile_resources(
	APP_ID + '.src',
	APP_ID + '.src.gresource.xml',
	gresource_bundle: true,
	install: true,
	install_dir : get_option('datadir') / APP_ID
)
```

### Flatpak

Finally, let's write the Flatpak manifest that will allow us to build and run the app in a well-known environment. Put this in the `org.example.filebrowser.yml` file:

```yaml
app-id: org.example.filebrowser
runtime: org.gnome.Platform
runtime-version: '43'
sdk: org.gnome.Sdk
command: org.example.filebrowser

finish-args:
  - --socket=wayland
  - --socket=fallback-x11
  - --share=ipc
  - --device=dri

cleanup:
  - /include
  - /lib/pkgconfig
  - /share/doc
  - /share/man
  - '*.a'
  - '*.la'

modules:

  - name: filebrowser
    buildsystem: meson
    sources:
      - type: dir
        path: .
```

## Building and Running Our Application

It's now time to test that our app is working! First install the necessary Flatpak runtime and SDK:

```sh {linenos=none}
flatpak install --user org.gnome.Platform//43 org.gnome.Sdk//43
```

Then install the [Flatpak Builder tool][Flatpak Builder]:

```sh {linenos=none}
flatpak install org.flatpak.Builder
```

Now we can build the application:

```sh {linenos=none}
flatpak run org.flatpak.Builder --force-clean --user --install build-dir org.example.filebrowser.yml
```

And run it:

```sh {linenos=none}
flatpak run org.example.filebrowser
```

If you did everything correctly, `Hello World!` should be printed in the console.

All this work for that? It's now time to do some more interesting things!


[Flatpak Builder]: https://docs.flatpak.org/en/latest/flatpak-builder-command-reference.html

[GNOME Application ID]: https://developer.gnome.org/documentation/tutorials/application-id.html

[Meson gnome]: https://mesonbuild.com/Gnome-module.html
[Meson gnome.compile_resources()]: https://mesonbuild.com/Gnome-module.html#gnomecompile_resources

[Gio.Resource]: https://docs.gtk.org/gio/struct.Resource.html
