---
title: Creating and Running a GTK Application
summary: Build a simple GTK4 application with GJS
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/gtk-application
weight: 2
slug: gtk-application
---

A [GTK application][Gtk.Application] will do a lot of platform integration for us, so that we can focus on our widgets. It is single-threaded and spins its own loop.

## Life Cycle

When the application is started, it will go through two different stages:

- _startup_, where we'll do all the initialization required
- _activation_, where we'll create the windows

An application is normally single-instance, meaning that as long as it is running, the _startup_ code will not be run again, while running it again will go directly to the _activation_ stage.

The application will exit when its last window is closed, or when you manually exit it. It will then go through a _shutdown_ stage.

## Our Own Application

We'll subclass the `Gtk.Application` class in order to implement the various stages.

Create a new `src/Application.js` file, and create the GObject `FbrApplication` class, extending the `Gtk.Application` class:

```js
import Gtk from 'gi://Gtk';
import GObject from 'gi://GObject';

export const FbrApplication = GObject.registerClass({
	GTypeName: 'FbrApplication'
}, class extends Gtk.Application {});
```

{{% info %}}
It's a good practice to prefix the objects to prevent name collisions. Here, we chose the `Fbr` prefix, for <b>F</b>ile <b>Br</b>owser.
{{% /info %}}

Right now we don't have any particular initialization to do, so we'll just implement the _activation_ stage. This is done by implementing the [`activate()` virtual method][Gio.Application.vfunc_activate()]. Later, if your application supports opening files, you'll also have to implement the [`open()` virtual method][Gio.Application.vfunc_open()].

To implement a virtual method, append `vfunc_` to its name, and add it to your class:

```js {linenostart=6,hl_lines="2-4"}
class extends Gtk.Application {
	vfunc_activate() {
		console.log('Hello World!');
	}
}
```

Remember to add `<file>Application.js</file>` in `src/org.example.filebrowser.src.gresource.xml`:

```xml {hl_lines=5}
<?xml version="1.0" encoding="UTF-8"?>
<gresources>
	<gresource prefix="/org/example/filebrowser/js">
		<file>main.js</file>
		<file>Application.js</file>
	</gresource>
</gresources>
```

In `src/main.js`, we'll ask for GTK4, then import our application and create a new instance with our application ID, instead of just logging a message:

```js
import 'gi://Gdk?version=4.0';
import 'gi://Gtk?version=4.0';

import { FbrApplication } from './Application.js';

export function main(argv) {
	return new FbrApplication({ 'application-id': pkg.name }).run(argv);
}
```

## Let's Display Widgets!

What's more exciting than displaying a message in the console? Displaying a message in a window!

GTK provides us with [a lot of widgets][GTK Visual Index]. Right now, two in particular will be useful: [`Gtk.ApplicationWindow`][Gtk.ApplicationWindow] and [`Gtk.Label`][Gtk.Label] The former is, obviously, a window attached to a specific application, and the latter displays text.

So, in `src/Application.js`, in our `activate` virtual method, we create a new window, create a new label, set the label as child of the window, and display the window:

```js {linenostart=7}
vfunc_activate() {
	const window = new Gtk.ApplicationWindow({ application: this });
	const label = new Gtk.Label({ label: 'Hello World!' });
	window.child = label;
	window.present();
}
```

Now, if you build and run your application, it will open a window with the message inside.

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window.png"
	src-2x="screenshots/window-2x.png"
	src-dark="screenshots/window-dark.png"
	src-dark-2x="screenshots/window-dark-2x.png"
>}}
<!-- spellchecker-enable -->

Notice that closing the window also exits the application, as it was its last window attached to the application.

Let's show more interesting things in this window!


[Gio.Application.vfunc_activate()]: https://docs.gtk.org/gio/vfunc.Application.activate.html
[Gio.Application.vfunc_open()]: https://docs.gtk.org/gio/vfunc.Application.open.html

[Gtk.Application]: https://docs.gtk.org/gtk4/class.Application.html
[Gtk.ApplicationWindow]: https://docs.gtk.org/gtk4/class.ApplicationWindow.html
[Gtk.Label]: https://docs.gtk.org/gtk4/class.Label.html

[GTK Visual Index]: https://docs.gtk.org/gtk4/visual_index.html
