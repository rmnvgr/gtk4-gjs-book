---
title: Commonly Used Widgets
summary: A list of useful GTK4 widgets and properties for displaying texts, images and interacting
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/common-widgets
weight: 3
slug: common-widgets
---

While GTK provides a lot of widgets, you'll find that you will often be using the same handful. Let's use them to make a welcome screen for our app!

## Box

The [`Gtk.Box`][Gtk.Box] is a layout widget: it puts any widget we append to it in line, horizontally or vertically.

In our application's `activate` virtual method, instead of setting a label as child of the window, we add a box that we'll populate later with other widgets:

```js {linenostart=8,hl_lines="2-3"}
const window = new Gtk.ApplicationWindow({ application: this });
const box = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
window.child = box;
window.present();
```

Running this will only give us an empty window. Let's add more widgets!

## Image

The [`Gtk.Image`][Gtk.Image] is useful if we want to display an icon. GTK comes with some default icons that we can load by name, we'll see in a later chapter [how to add our own icons][Custom Icons]. For this example, we'll use the built-in `system-file-manager-symbolic` icon name.

{{% info %}}
To browse the icons available on your system, you can install the [Icon Library application][Icon Library App] and scroll to the <!-- spellchecker-disable -->"Pre-Installed System Icons"<!-- spellchecker-enable --> section.
{{% /info %}}

Let's create the image and append it to the box:

```js {linenostart=7,hl_lines="5-9"}
vfunc_activate() {
	const window = new Gtk.ApplicationWindow({ application: this });
	const box = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });

	const image = new Gtk.Image({
		iconName: 'system-file-manager-symbolic',
		iconSize: Gtk.IconSize.LARGE
	});
	box.append(image);

	window.child = box;
	window.present();
}
```

## Label

We already crossed the path of the [`Gtk.Label`][Gtk.Label] widget in the previous chapter. It displays any text we want:

```js {linenostart=7,hl_lines="4-8"}
vfunc_activate() {
	/* ... */

	const label = new Gtk.Label({
		label: 'Welcome to our new file browser!',
		wrap: true
	});
	box.append(label);

	window.child = box;
	window.present();
}
```

This time we made the label able to wrap by setting its [`wrap` property][Gtk.Label:wrap], so that it can take multiple lines if the window is smaller than its size.

## Button

For good measure, let's add a [`Gtk.Button`][Gtk.Button], which is… a button. For now it will just sit there, but we'll make it more useful when we explore signals and actions.

```js {linenostart=7,hl_lines="4-7"}
vfunc_activate() {
	/* ... */

	const button = new Gtk.Button({
		label: 'Let\'s go!'
	});
	box.append(button);

	window.child = box;
	window.present();
}
```

## Spacing and Alignment

If you run the application, you'll see that our widgets are all stuck together, and when you increase the window's height, they don't get centered. Furthermore, the button takes the whole window width.

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window-1.png"
	src-2x="screenshots/window-1-2x.png"
	src-dark="screenshots/window-1-dark.png"
	src-dark-2x="screenshots/window-1-dark-2x.png"
>}}
<!-- spellchecker-enable -->

Fortunately, there's a few properties we can tweak to improve this!

### Margins

The easiest way to add space around our widgets is to set their margins. We can do this with the [`margin-top`][Gtk.Widget:margin-top], [`margin-bottom`][Gtk.Widget:margin-bottom], [`margin-start`][Gtk.Widget:margin-start] and [`margin-end`][Gtk.Widget:margin-end] properties. Later we'll also [do it with CSS][Styling Widgets With CSS]. Let's add margins to our box:

```js {linenostart=9}
const box = new Gtk.Box({
	orientation: Gtk.Orientation.VERTICAL,
	marginTop: 36,
	marginBottom: 36,
	marginStart: 36,
	marginEnd: 36
});
```

### Spacing

Our box is now breathing, but the children inside are still stuck to each other. We could also set margins for every one of them, but boxes have a special property: [`spacing`][Gtk.Box:spacing] It add spacing between the box's children!

```js {linenostart=9,hl_lines=3}
const box = new Gtk.Box({
	/* ... */
	spacing: 18
});
```

### Alignment

As we saw, when resizing the window, the box is not centered while the button takes the full width. To change that, we'll use the [`halign`][Gtk.Widget:halign] and [`valign`][Gtk.Widget:valign] properties, that will set the horizontal and vertical alignment, respectively:

```js {linenostart=9,hl_lines=3}
const box = new Gtk.Box({
	/* ... */
	valign: Gtk.Align.CENTER
});
```

```js {linenostart=31,hl_lines=3}
const button = new Gtk.Button({
	label: 'Let\'s go!',
	halign: Gtk.Align.CENTER
});
```

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window-2.png"
	src-2x="screenshots/window-2-2x.png"
	src-dark="screenshots/window-2-dark.png"
	src-dark-2x="screenshots/window-2-dark-2x.png"
>}}
<!-- spellchecker-enable -->

Our window is now beautiful, but our code is getting quite messy. It'll be worse when we add real logic here! So it's time to split our presentation code into separate files.


[Custom Icons]: {{< ref "/application/07.icons/index.md" >}}
[Styling Widgets With CSS]: {{< ref "/application/06.css/index.md" >}}

[Gtk.Box]: https://docs.gtk.org/gtk4/class.Box.html
[Gtk.Box:spacing]: https://docs.gtk.org/gtk4/property.Box.spacing.html
[Gtk.Button]: https://docs.gtk.org/gtk4/class.Button.html
[Gtk.Image]: https://docs.gtk.org/gtk4/class.Image.html
[Gtk.Label]: https://docs.gtk.org/gtk4/class.Label.html
[Gtk.Label:wrap]: https://docs.gtk.org/gtk4/property.Label.wrap.html
[Gtk.Widget:halign]: https://docs.gtk.org/gtk4/property.Widget.halign.html
[Gtk.Widget:margin-bottom]: https://docs.gtk.org/gtk4/property.Widget.margin-bottom.html
[Gtk.Widget:margin-end]: https://docs.gtk.org/gtk4/property.Widget.margin-end.html
[Gtk.Widget:margin-start]: https://docs.gtk.org/gtk4/property.Widget.margin-start.html
[Gtk.Widget:margin-top]: https://docs.gtk.org/gtk4/property.Widget.margin-top.html
[Gtk.Widget:valign]: https://docs.gtk.org/gtk4/property.Widget.valign.html

[Icon Library App]: https://flathub.org/apps/details/org.gnome.design.IconLibrary
