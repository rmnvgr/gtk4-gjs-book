---
title: UI Templates and Composite Widgets
summary: Create powerful composite widgets with GTK4 UI templates and GJS
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/ui-templates-composite-widgets
weight: 4
slug: ui-templates-composite-widgets
---

To build user interfaces, GTK can read XML files in [a format it understands][Gtk.Builder]. Here are the most useful tags:

- `<interface/>`: It's the top level element, it is mandatory.
- `<template class=""/>`: Direct child of the `interface` element, it tells GTK that we are defining the template for our custom widget, whose name we'll put in the `class` attribute.
- `<object class=""/>`: Create a new object from the class defined in the attribute of the same name, for example `GtkLabel` (without the dot after the namespace). It can optionally take an `id` attribute if you need to access the object elsewhere.
- `<property name=""/>`: Direct child of a `template` or `object` element, sets the property defined in the `name` attribute of the parent object to the value inside the element. For enumeration values, like `Gtk.Orientation.VERTICAL`, you can simply use `vertical`.
- `<child/>`: Direct child of a `template` or `object` element, adds the object defined inside as a child of the parent object.

Other tags exists, it's recommended to read the full documentation of the [`Gtk.Builder`][Gtk.Builder#ui-definitions] and [`Gtk.Widget`][Gtk.Widget#as-buildable] classes on this topic, as well as the documentation of each GTK widget for specific elements.

## Defining Our Widget in UI XML

Let's take our welcome widgets from the previous chapter. We'll translate them from code to a UI template. We start by creating a `data/ui/WelcomeWidget.ui` file and put this mandatory content:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
</interface>
```

Don't forget to add this new file in `data/org.example.filebrowser.data.gresource.xml`:

```xml {hl_lines=4}
<?xml version="1.0" encoding="UTF-8"?>
<gresources>
	<gresource prefix="/org/example/filebrowser">
		<file>ui/WelcomeWidget.ui</file>
	</gresource>
</gresources>
```

Now we define a new `FbrWelcomeWidget` template:

```xml {hl_lines="3-4"}
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<template class="FbrWelcomeWidget">
	</template>
</interface>
```

Our widget will inherit from the `Gtk.Widget` class. In code, we used a `Gtk.Box` as the base widget. We could inherit from it, but it's recommended to inherit from `Gtk.Widget`. To make our widget behave like a box, we set its [layout manager][Gtk.Widget:layout-manager] to a [`Gtk.BoxLayout`][Gtk.BoxLayout]. [Other layout managers exist][Gtk.LayoutManager#descendants], depending on what you need.

We add a new `object` element, with its attribute `class` set to `GtkBoxLayout`. Inside, we define its `orientation` and `spacing` properties to the values we set previously in code, and we set it as the value of the `layout-manager` property of our template:

```xml {linenostart=3,hl_lines="2-7"}
<template class="FbrWelcomeWidget">
	<property name="layout-manager">
		<object class="GtkBoxLayout">
			<property name="orientation">vertical</property>
			<property name="spacing">18</property>
		</object>
	</property>
</template>
```

Now we can add our children! We had a `Gtk.Image`, a `Gtk.Label` and a `Gtk.Button`. We create these three objects, set their properties, and add them each in a `child` element in our template:

```xml {linenostart=3,hl_lines="3-21"}
<template class="FbrWelcomeWidget">
	<!-- ... -->
	<child>
		<object class="GtkImage">
			<property name="icon-name">system-file-manager-symbolic</property>
			<property name="icon-size">large</property>
		</object>
	</child>
	<child>
		<object class="GtkLabel">
			<property name="label">Welcome to our new file browser!</property>
			<property name="wrap">true</property>
			<property name="justify">center</property>
		</object>
	</child>
	<child>
		<object class="GtkButton">
			<property name="label">Let's go!</property>
			<property name="halign">center</property>
		</object>
	</child>
</template>
```

Our template is ready! But in order to use it, we must define a composite widget.

## Creating a Composite Widget

A composite widget is simply a custom widget built with a template.

First we create our custom widget. In a new `src/WelcomeWidget.js` file (and don't forget to add it in the resources file), we register a new GObject, inheriting the base `Gtk.Widget`:

```js
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

export const WelcomeWidget = GObject.registerClass({}, class extends Gtk.Widget {});
```

Of course, this won't do much. The first argument of the `registerClass` method is an object that can define some aspects of our widget. For now we'll use two of them: `GTypeName`, which tells GObject the name of our widget so it can load it and use it in other UI templates, and `Template`, which tells GTK to load our UI template. Because it's stored in a resource, we use the `resource://` protocol and give the full path inside the resource.

```js {linenostart=4,hl_lines="2-3"}
export const WelcomeWidget = GObject.registerClass({
	GTypeName: 'FbrWelcomeWidget',
	Template: 'resource:///org/example/filebrowser/ui/WelcomeWidget.ui'
}, class extends Gtk.Widget {});
```

## Using Custom Widgets in UI Templates

We can now use our custom widget in JavaScript, with `new Widget()`. And even more useful, we can use it in other UI templates!

Let's quickly create a new custom widget for our window, directly inheriting from `Gtk.ApplicationWindow` (don't forget to add the new files to the resources lists).

In `src/Window.js` we create our class and load the template:

```js
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

export const Window = GObject.registerClass({
	GTypeName: 'FbrWindow',
	Template: 'resource:///org/example/filebrowser/ui/Window.ui',
}, class extends Gtk.ApplicationWindow {});
```

In the [`close_request()` virtual function][Gtk.Window.vfunc_close_request()], which is called when the window is closed, we clean up all the objects it may be holding:

```js {linenostart=7,hl_lines="2-5"}
class extends Gtk.ApplicationWindow {
	vfunc_close_request() {
		super.vfunc_close_request();
		this.run_dispose();
	}
}
```

In `data/ui/Window.ui` we define the template, doing what we were previously doing in code, setting the `child` property of the window and margins and alignment of the widget:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<template class="FbrWindow">
		<property name="child">
			<object class="FbrWelcomeWidget">
				<property name="margin-top">36</property>
				<property name="margin-bottom">36</property>
				<property name="margin-start">36</property>
				<property name="margin-end">36</property>
				<property name="valign">center</property>
			</object>
		</property>
	</template>
</interface>
```

Now, in `src/Application.js`, we have to import our new widgets (so GObject is aware of their existence), and show our new `Window` widget:

```js {hl_lines=["4-5",12]}
import Gtk from 'gi://Gtk';
import GObject from 'gi://GObject';

import './WelcomeWidget.js';
import { Window } from './Window.js';


export const Application = GObject.registerClass({
	GTypeName: 'FbrApplication'
}, class extends Gtk.Application {
	vfunc_activate() {
		const window = new Window({ application: this });
		window.present();
	}
});
```

---

If you build and run our application, it should look exactly like what we had before. But now, when the code grows as we add more logic, it is clearly separated from the structure of our user interface! However, we can still go further by also separating the styling from the widget hierarchy.


[Gtk.BoxLayout]: https://docs.gtk.org/gtk4/class.BoxLayout.html
[Gtk.Builder]: https://docs.gtk.org/gtk4/class.Builder.html
[Gtk.Builder#ui-definitions]: https://docs.gtk.org/gtk4/class.Builder.html#gtkbuilder-ui-definitions
[Gtk.LayoutManager#descendants]: https://docs.gtk.org/gtk4/class.LayoutManager.html#descendants
[Gtk.Widget#as-buildable]: https://docs.gtk.org/gtk4/class.Widget.html#gtkwidget-as-gtkbuildable
[Gtk.Widget:layout-manager]: https://docs.gtk.org/gtk4/property.Widget.layout-manager.html
[Gtk.Window.vfunc_close_request()]: https://docs.gtk.org/gtk4/vfunc.Window.close_request.html
