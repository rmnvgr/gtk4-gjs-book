---
title: Custom Properties and Property Binding
summary: Add custom properties to GObjects and widgets, and bind them in UI templates
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/property-binding
weight: 5
slug: property-binding
---

Our Welcome widget is nice, but as-is not really reusable elsewhere, as we hard-code different values, for example the icon name or the welcome text.

Instead, we can make them properties of our widget, which means we can then change them from the outside, like the `label` property of `Gtk.Label`.

## Adding Properties

To add properties, we need to pass a `Properties` object to the configuration object, with each key the property and each value a [`GObject.ParamSpec` instance][GObject.ParamSpec] of the desired type.

For instance, we will add a `welcome-text` property. This property will be a string, so to create the corresponding [`GObject.ParamSpecString` object][GObject.ParamSpecString] that describes it, we will use the [`GObject.ParamsSpec.string()` function][GObject.param_spec_string()]. It takes several values:

- `name`: The name of the property, which must be in kebab case
- `nick`: The human-friendly name
- `blurb`: The description of the property
- `default_value`: The default value of the property
- `flags`: The [`GObject.ParamFlags`][GObject.ParamFlags] describing the property

Be careful, in GJS, the function signature is slightly different than in the documentation and you must always pass the flags after the blurb.

{{% info %}}
The GJS Guide has a [section dedicated to properties][GJS GObject Subclasses#properties] with numerous examples of the possible types.
{{% /info %}}

But let's translate all that in code, in `src/WelcomeWidget.js`:

```js {linenostart=4,hl_lines="4-12"}
export const WelcomeWidget = GObject.registerClass({
	GTypeName: 'FbrWelcomeWidget',
	Template: 'resource:///org/example/filebrowser/ui/WelcomeWidget.ui',
	Properties: {
		WelcomeText: GObject.ParamSpec.string(
			'welcome-text', // name
			'Welcome Text', // nick
			'The text displayed by the widget', // blurb
			GObject.ParamFlags.READWRITE, // flags
			'' // default value
		),
	},
}, class extends Gtk.Widget {});`
```

## Binding Properties in the UI Template

Now, we have to tell the UI template that it must use the property value instead of the hard-coded value. For that, we'll use [property binding][GObject.Binding].

To do so, instead of putting a value in the `property` element, we will give it several attributes:

- `bind-source`: The source object that bears the property we're interested in, either its name if it's the template widget or its ID
- `bind-property`: The property we want to bind
- `bind-flags`: Optionally, the [`GObject.BindingFlags`][GObject.BindingFlags] used to bind the property

Let's bind the `label` property of the label inside our widget to the `welcome-text` property! In `data/ui/WelcomeWidget.ui`:

```xml {linenostart=17,hl_lines=2}
<object class="GtkLabel">
	<property name="label" bind-source="FbrWelcomeWidget" bind-property="welcome-text" bind-flags="sync-create"/>
	<!-- ... -->
</object>
```

And we set the text we want when we instance the widget in `data/ui/Window.ui`:

```xml {linenostart=5,hl_lines=2}
<object class="FbrWelcomeWidget">
	<property name="welcome-text">Welcome to our new file browser!</property>
	<!-- ... -->
</object>
```

Thanks to our changes, if we need to reuse this widget elsewhere with a different text, it's as easy as setting a different value for the `welcome-text` property, instead of creating a whole new custom widget. Of course, you can also make the icon name and the button label use additional properties!

{{% info %}}
To bind properties from code, it's possible to use the [`bind_property()` function][GObject.Object.bind_property()]:

```js {linenos=false}
sourceObject.bind_property('source-property', targetObject, 'target-property', bindFlags);
```
{{% /info %}}

## Getters and Setters

There's just a slight problem with our custom property: if the welcome text is empty, there's too much spacing between the icon and the button, because the label is still there. To fix that, we have to hide it if the text is empty, and show it if there's some text.

Doing that is easy: if we implement the getter and setter of the property, we can do other things there. We must be careful to call the [`notify()` function][GObject.Object.notify()] in our setter if the value changes so that other objects connected to the property know that it changed.

In `src/WelcomeWidget.js`:

```js {linenostart=16,hl_lines="2-12"}
class extends Gtk.Widget {
	get welcomeText() {
		// Return a default value if the text hasn't been set yet
		return this._welcomeText || '';
	}

	set welcomeText(value) {
		// Do nothing if the new value is the same as the old one
		if (this._welcomeText === value)
			return;
		// Store the value in an internal property
		this._welcomeText = value;
		// Notify that the value has changed
		this.notify('welcome-text');
	}
}
```

To access our label in the code, we give it an ID in the UI template. And because the default value of the `welcome-text` property is empty, we hide it:

```xml {linenostart=17,hl_lines="1-2"}
<object class="GtkLabel" id="welcomeLabel">
	<property name="visible">false</property>
	<!-- ... -->
</object>
```

And add it to the `InternalChildren` array of the configuration object:

```js {linenostart=4,hl_lines=3}
export const WelcomeWidget = GObject.registerClass({
	/* ... */
	InternalChildren: ['welcomeLabel'],
}, class extends Gtk.Widget {/* ... */});
```

Now, we can easily set its `visible` property in the setter of the `welcome-text` property:

```js {linenostart=23,hl_lines="3-4"}
set welcomeText(value) {
	/* ... */
	// Hide the label if no text is set
	this._welcomeLabel.visible = !!value;
	// Notify that the value has changed
	this.notify('welcome-text');
}
```

Try not setting the `welcome-text` property when you instantiate the widget: there shouldn't be any extra space.

---

Property bindings are a really powerful feature as they allow widgets to react to other widgets entirely from UI templates, without writing complex code!


[GJS GObject Subclasses#properties]: https://gjs.guide/guides/gobject/subclassing.html#properties

[GObject.Binding]: https://docs.gtk.org/gobject/class.Binding.html
[GObject.BindingFlags]: https://docs.gtk.org/gobject/flags.BindingFlags.html
[GObject.Object.bind_property()]: https://docs.gtk.org/gobject/method.Object.bind_property.html
[GObject.Object.notify()]: https://docs.gtk.org/gobject/method.Object.notify.html
[GObject.ParamFlags]: https://docs.gtk.org/gobject/flags.ParamFlags.html
[GObject.ParamSpec]: https://docs.gtk.org/gobject/class.ParamSpec.html
[GObject.ParamSpecString]: https://docs.gtk.org/gobject/class.ParamSpecString.html
[GObject.param_spec_string()]: https://docs.gtk.org/gobject/func.param_spec_string.html
