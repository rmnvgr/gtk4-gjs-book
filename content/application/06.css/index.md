---
title: Styling Widgets With CSS
summary: Use CSS styling to improve the appearance of widgets
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/css
weight: 6
slug: css
---

[CSS] is the presentation language of the web, allowing to change colors, spacing, fonts, borders, shadows of elements and so much more. And we can use it with GTK too!

However, not all properties are supported. You can find a list of the supported properties in the [official documentation][GTK CSS Properties].

## Loading a Stylesheet

We start by creating a `style.css` file in `data/css/`:

```css
window {
	background-color: orchid;
}
```

For the moment we only put a dummy rule inside, to check when our stylesheet is loaded.

We add it to our data resources:

```xml {linenostart=4}
<file>css/style.css</file>
```

Now, we need to load it when our application starts, so we will run code in [the `startup` virtual method][Gio.Application.vfunc_startup()]. Styling information in GTK is managed by a [`Gtk.StyleContext` object][Gtk.StyleContext], so we will create one for the default display, and then add as a provider a [`Gtk.CssProvider` object][Gtk.CssProvider] loaded with our stylesheet.

Which gives, translated in code, in `src/Application.js`:

```js
import Gdk from 'gi://Gdk';
```

```js {linenostart=10,hl_lines=["2-5","9-20"]}
class extends Gtk.Application {
	vfunc_startup() {
		super.vfunc_startup();
		this.#loadStylesheet();
	}

	/* ... */

	#loadStylesheet() {
		// Load the stylesheet in a CssProvider
		const provider = new Gtk.CssProvider();
		provider.load_from_resource('/org/example/filebrowser/css/style.css');

		// Add the provider to the StyleContext of the default display
		Gtk.StyleContext.add_provider_for_display(
			Gdk.Display.get_default(),
			provider,
			Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
		);
	}
}
```

Now, when we run the application, its background should be purple-y.

## Styling Our Welcome Widget

Let's use CSS to replace the styling information we put in our UI files.

First, we have to give our Welcome widget a CSS name, so that we can target it in our stylesheet. By default, it gets the `widget` name, which is not very helpful. To change it, we set the `CssName` property of the configuration object of the `GObject.registerClass()` function:

```js {linenostart=4,hl_lines=3}
export const WelcomeWidget = GObject.registerClass({
	GTypeName: 'FbrWelcomeWidget',
	CssName: 'welcome',
	Template: 'resource:///org/example/filebrowser/ui/WelcomeWidget.ui',
	/* ... */
}, class extends Gtk.Widget {});
```

We then remove the `spacing` property of the layout manager in `data/ui/WelcomeWidget.ui`:

```xml {linenostart=5}
<object class="GtkBoxLayout">
	<property name="orientation">vertical</property>
</object>
```

And all the `margin-*` properties of the widget in `data/ui/Window.ui`:

```xml {linenostart=5}
<object class="FbrWelcomeWidget">
	<property name="welcome-text">Welcome to our new file browser!</property>
	<property name="valign">center</property>
</object>
```

The `spacing` property can be replaced by the [`border-spacing` CSS property][CSS border-spacing]. The different `margin-*` properties can be replaced by the [shorthand `margin` CSS property][CSS margin]. Remove the rules in the `style.css` file and add these ones for the Welcome widget:

```css
welcome {
	border-spacing: 18px;
	margin: 36px;
}
```

We can go further, and for instance change the font size and weight of the label inside. The CSS name of each widget can be found in the "CSS Nodes" section of its documentation page. So, for a `Gtk.Label`, [we can see][Gtk.Label#css-nodes] that the CSS name is `label`.

To only change the appearance of the label directly inside our widget (in order to not change other labels such as the one in the button), we use [the child combinator selector][CSS child combinator]:

```css {linenostart=6}
welcome > label {
	font-size: 1.4em;
	font-weight: bold;
}
```

## CSS Classes

To give widgets different appearances depending on the context (for example, allowing different font sizes for our Welcome widget), we can use [CSS classes].

To give a widget a class in the UI file, we use the `style` element, and add a `class` element with the `name` attribute set to the class we want. A widget can have multiple classes

Let's add the `big` class to the Welcome widget instance of the main window in `data/ui/Window.ui` (removing the margins in the meantime):

```xml {linenostart=5,hl_lines="3-5"}
<object class="FbrWelcomeWidget">
	<!-- ... -->
	<style>
		<class name="big"/>
	</style>
</object>
```

And update the selector for the label inside our widget in `data/css/style.css`:

```css {linenostart=6,hl_lines=1}
welcome.big > label {
	/* ... */
}
```

Now, the text of our widget will only be bigger when it has the `big` class.

## Built-In Classes

Some widgets have built-in classes that you can use. They are mentioned in the "CSS Nodes" section of their documentation page.

For instance, [we can read][Gtk.Button#css-nodes] that the `Gtk.Button` object can have the `suggested-action` class. We can use it on the button of our widget to give it more impact.

Let's add this class to the button in `data/ui/WelcomeWidget.ui`:

```xml {linenostart=24,hl_lines="3-5"}
<object class="GtkButton">
	<!-- ... -->
	<style>
		<class name="suggested-action"/>
	</style>
</object>
```

The button now has a blue background, inviting the user to click on it.

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window.png"
	src-2x="screenshots/window-2x.png"
	src-dark="screenshots/window-dark.png"
	src-dark-2x="screenshots/window-dark-2x.png"
>}}
<!-- spellchecker-enable -->

---

CSS in GTK is very powerful, it also has some advanced features such as color definitions and expressions and some custom properties, you can read about these in the [official documentation][GTK CSS Properties]. Use it to make your application stand out!


[CSS]: https://developer.mozilla.org/docs/Web/CSS
[CSS border-spacing]: https://developer.mozilla.org/docs/Web/CSS/border-spacing
[CSS child combinator]: https://developer.mozilla.org/docs/Web/CSS/Child_combinator
[CSS classes]: https://developer.mozilla.org/docs/Web/CSS/Class_selectors
[CSS margin]: https://developer.mozilla.org/docs/Web/CSS/margin

[Gio.Application.vfunc_startup()]: https://docs.gtk.org/gio/vfunc.Application.startup.html

[Gtk.Button#css-nodes]: https://docs.gtk.org/gtk4/class.Button.html#css-nodes
[Gtk.CssProvider]: https://docs.gtk.org/gtk4/class.CssProvider.html
[Gtk.Label#css-nodes]: https://docs.gtk.org/gtk4/class.Label.html#css-nodes
[Gtk.StyleContext]: https://docs.gtk.org/gtk4/class.StyleContext.html

[GTK CSS Properties]: https://docs.gtk.org/gtk4/css-properties.html
