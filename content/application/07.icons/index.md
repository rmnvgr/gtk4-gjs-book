---
title: Custom Icons
summary: Add custom symbolic icons to a GTK4 application
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/icons
weight: 7
slug: icons
---

GTK ships with a good number of built-in icons, however they can't fulfill all the needs. That's where custom icons come into play!

There are two kind of icons:

- <!-- spellchecker-disable -->{{< svg-use class="icon" width="16" height="16" id="emoji" >}}<!-- spellchecker-enable --> Full color icons, that will be displayed as-is
- <!-- spellchecker-disable -->{{< svg-use class="icon" width="16" height="16" id="emoji-symbolic" >}}<!-- spellchecker-enable --> Symbolic icons, that will be recolored to the text color, allowing them to adapt to any style

It's almost always recommended to use symbolic icons. GTK knows that an icon is symbolic if its name is prefixed with `-symbolic`.

The preferred format is [SVG][SVG], so they can be scaled to any size.

Icons must be stored in a specific hierarchy, respecting the [Icon Theme specification][XDG Icon Theme]: `$ICON_THEME/$SIZE/$CONTEXT/`. The default icon theme is `hicolor`, for scalable SVG the size is `scalable`, and the context is one of the contexts defined in the [Icon Naming specification][XDG Icon Naming].

GTK will automatically load icons in resources if they are placed with the above hierarchy (without the icon theme) in the `icons` directory under the application base resource path.

## Creating a Custom Symbolic Icon

Scalable symbolic icons should be a 16px by 16px SVG image, they can be drawn with [Inkscape][Inkscape]. The GNOME Human Interface Guidelines have [a chapter dedicated to drawing nice symbolic icons][GNOME HIG UI Icons].

We'll replace the icon in our Welcome widget with this custom icon: <!-- spellchecker-disable -->{{< svg-use class="icon" width="16" height="16" id="welcome" >}}<!-- spellchecker-enable -->

Let's put the SVG code in `data/icons/welcome-symbolic.svg`:

```svg
<?xml version="1.0" encoding="UTF-8"?>
<svg width="16" height="16" version="1.1" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
	<path d="m8 1a1.0001 1.0001 0 0 0-0.58203 0.1875l-7 5a1 1 0 0 0-0.23242 1.3965 1 1 0 0 0 1.3965 0.23242l0.41797-0.29883v4.4844a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2v-4.4844l0.41797 0.29883a1 1 0 0 0 1.3965-0.23242 1 1 0 0 0-0.23242-1.3965l-7-5a1.0001 1.0001 0 0 0-0.58203-0.1875zm-2 7.002h3v4h-3z"/>
</svg>
```

{{% info %}}
It's possible to use special colors from the theme by adding the `success`, `warning` or `error` class to an element.
{{% /info %}}

## Bundling Custom Icons

In order for GTK to automatically load our icon, we must put it in a special location in our resources: `/org/example/filebrowser/icons/scalable/actions`.

To do that, we add a new `gresource` element in `data/org.example.filebrowser.data.gresource.xml` with the above prefix:

```xml {linenostart=2,hl_lines="5-6"}
<gresources>
	<gresource prefix="/org/example/filebrowser">
		<!-- ... -->
	</gresource>
	<gresource prefix="/org/example/filebrowser/icons/scalable/actions">
	</gresource>	
</gresources>
```

Now, instead of simply adding a `file` element as before, we use the `alias` attribute to rename it. That will avoid putting the icon in an `icons` sub-directory inside `actions` (because our file is loaded from the `icons` directory), and we'll use that opportunity to prefix the icon name with the application name, so that it doesn't get replaced by icons from another application that use the same name.

```xml {linenostart=8,hl_lines=2}
<gresource prefix="/org/example/filebrowser/icons/scalable/actions">
	<file alias="filebrowser-welcome-symbolic.svg">icons/welcome-symbolic.svg</file>
</gresource>
```

## Using a Custom Icon

This is as simple as using the icon name!

So, in `data/ui/WelcomeWidget.ui`, we replace the built-in icon name by our custom icon name:

```xml {linenostart=10,hl_lines="2"}
<object class="GtkImage">
	<property name="icon-name">filebrowser-welcome-symbolic</property>
	<!-- ... -->
</object>
```

And that's it! Our Welcome widget now uses our custom icon.

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window.png"
	src-2x="screenshots/window-2x.png"
	src-dark="screenshots/window-dark.png"
	src-dark-2x="screenshots/window-dark-2x.png"
>}}
<!-- spellchecker-enable -->


<!-- Icon definitions -->

<!-- spellchecker-disable -->
{{< svg-symbol viewBox="0 0 16 16" id="emoji" >}}
<defs>
	<linearGradient id="shade" x1="0.4" x2="0.6" y1="0" y2="1">
		<stop stop-color="#ffd75f" offset="0"/>
		<stop stop-color="#bf8804" offset="1"/>
	</linearGradient>
</defs>
<circle cx="8" cy="8" r="7" fill="url(#shade) #efb323"/>
<path d="m6 5c0.55228 0 1 0.67157 1 1.5s0 1.5-1 1.5-1-0.67157-1-1.5 0.44772-1.5 1-1.5z"/>
<path d="m10 5c0.55228 0 1 0.67157 1 1.5s0 1.5-1 1.5-1-0.67157-1-1.5 0.44772-1.5 1-1.5z"/>
<path d="m4.4238 10h7.1523c0.55228 0 0.82822 0.4196 0.51172 0.87109-0.73338 1.0462-1.8419 1.7721-3.0938 2.0273-0.54025 0.11017-1.448 0.11017-1.9883 0-1.2519-0.25528-2.3604-0.98116-3.0938-2.0273-0.31649-0.45148-0.040566-0.87109 0.51172-0.87109z" fill="#fff"/>
{{< /svg-symbol >}}

{{< svg-symbol viewBox="0 0 16 16" id="emoji-symbolic" >}}
<path d="m8 1c-3.866 0-7 3.134-7 7s3.134 7 7 7 7-3.134 7-7-3.134-7-7-7zm-2 4c0.55228 0 1 0.67157 1 1.5s0 1.5-1 1.5-1-0.67157-1-1.5 0.44772-1.5 1-1.5zm4 0c0.55228 0 1 0.67157 1 1.5s0 1.5-1 1.5-1-0.67157-1-1.5 0.44772-1.5 1-1.5zm-5.5762 5h7.1523c0.55228 0 0.82822 0.4196 0.51172 0.87109-0.73338 1.0462-1.8419 1.7721-3.0938 2.0273-0.54025 0.11017-1.448 0.11017-1.9883 0-1.2519-0.25528-2.3604-0.98116-3.0938-2.0273-0.31649-0.45148-0.040566-0.87109 0.51172-0.87109z" fill="currentColor"/>
{{< /svg-symbol >}}

{{< svg-symbol viewBox="0 0 16 16" id="welcome" >}}
<path d="m8 1a1.0001 1.0001 0 0 0-0.58203 0.1875l-7 5a1 1 0 0 0-0.23242 1.3965 1 1 0 0 0 1.3965 0.23242l0.41797-0.29883v4.4844a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2v-4.4844l0.41797 0.29883a1 1 0 0 0 1.3965-0.23242 1 1 0 0 0-0.23242-1.3965l-7-5a1.0001 1.0001 0 0 0-0.58203-0.1875zm-2 7.002h3v4h-3z" fill="currentColor"/>
{{< /svg-symbol >}}
<!-- spellchecker-enable -->


[GNOME HIG UI Icons]: https://developer.gnome.org/hig/guidelines/ui-icons.html

[Inkscape]: https://inkscape.org/

[SVG]: https://developer.mozilla.org/docs/Web/SVG

[XDG Icon Naming]: https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
[XDG Icon Theme]: https://specifications.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html
