---
title: Changing Views
summary: Use a stack to switch between multiple views in your GTK4+GJS application
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/views
weight: 8
slug: views
---

Now that our application has a welcome screen, it would be great to switch to another view with our actual functionalities.

The best widget for that is a [`Gtk.Stack`][Gtk.Stack]. It displays only one of its children at a time, and allows you to switch between them. However, it doesn't accept any widget: it must be a [`Gtk.StackPage`][Gtk.StackPage], which will hold some information about the page, and have your widget as a child.

## Setting Up the Stack

Let's update our `data/ui/Window.ui` file to add a stack:

```xml {linenostart=3,hl_lines="3-4"}
<template class="FbrWindow">
	<property name="child">
		<object class="GtkStack" id="viewStack">
		</object>
	</property>
</template>
```

And we put the welcome widget in a stack page:

```xml {linenostart=5,hl_lines="2-15"}
<object class="GtkStack" id="viewStack">
	<child>
		<object class="GtkStackPage">
			<property name="name">welcome</property>
			<property name="child">
				<object class="FbrWelcomeWidget">
					<property name="welcome-text">Welcome to our new file browser!</property>
					<property name="valign">center</property>
					<style>
						<class name="big"/>
					</style>
				</object>
			</property>
		</object>
	</child>
</object>
```

The [`name` property][Gtk.StackPage:name] of the page is important: this is what will later allow us to choose which page to display, thanks to the stack [`visible-child-name` property][Gtk.Stack:visible-child-name].

We can now add a second page to the stack:

```xml {linenostart=5,hl_lines="3-12"}
<object class="GtkStack" id="viewStack">
	<!-- ... -->
	<child>
		<object class="GtkStackPage">
			<property name="name">files</property>
			<property name="child">
				<object class="GtkLabel">
					<property name="label">We will display files here</property>
				</object>
			</property>
		</object>
	</child>
</object>
```

## Switching Pages

Now that we have our stack, how do we change the visible page? As we saw earlier, it is possible to change the `visible-child-name` property of the stack to the name of the page we want to display.

We can also use a ready-made widget: the [`Gtk.StackSwitcher`][Gtk.StackSwitcher]. It will create a button for each page.

Let's put it in the window's title bar. Because we will override the default title bar, we have to put it inside the [`title-widget` property][Gtk.Headerbar:title-widget] of a [`Gtk.HeaderBar`][Gtk.HeaderBar] widget:

```xml {linenostart=3,hl_lines="2-10"}
<template class="FbrWindow">
	<property name="titlebar">
		<object class="GtkHeaderBar">
			<property name="title-widget">
				<object class="GtkStackSwitcher">
					<property name="stack">viewStack</property>
				</object>
			</property>
		</object>
	</property>
	<!-- ... -->
</template>
```

However, if you run the application now, the header bar will be empty. This is because we didn't provide any information about how to display our pages!

We can either set the title of the page with the [`title` property][Gtk.StackPage:title], or its icon with the [`icon-name` property][Gtk.StackPage.icon-name], which accepts the same names as the `Gtk.Image` we previously used.

Let's set the titles:

```xml {linenostart=16,hl_lines=3}
<object class="GtkStackPage">
	<property name="name">welcome</property>
	<property name="title">Welcome</property>
	<!-- ... -->
</object>
```

```xml {linenostart=31,hl_lines=3}
<object class="GtkStackPage">
	<property name="name">files</property>
	<property name="title">Files</property>
	<!-- ... -->
</object>
```

Now, when we run our application, the stack switcher displays our pages, and clicking it changes the view!

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window.png"
	src-2x="screenshots/window-2x.png"
	src-dark="screenshots/window-dark.png"
	src-dark-2x="screenshots/window-dark-2x.png"
>}}
<!-- spellchecker-enable -->


[Gtk.HeaderBar]: https://docs.gtk.org/gtk4/class.HeaderBar.html
[Gtk.Headerbar:title-widget]: https://docs.gtk.org/gtk4/property.HeaderBar.title-widget.html
[Gtk.Stack]: https://docs.gtk.org/gtk4/class.Stack.html
[Gtk.Stack:visible-child-name]: https://docs.gtk.org/gtk4/property.Stack.visible-child-name.html
[Gtk.StackPage]: https://docs.gtk.org/gtk4/class.StackPage.html
[Gtk.StackPage.icon-name]: https://docs.gtk.org/gtk4/property.StackPage.icon-name.html
[Gtk.StackPage:name]: https://docs.gtk.org/gtk4/property.StackPage.name.html
[Gtk.StackPage:title]: https://docs.gtk.org/gtk4/property.StackPage.title.html
[Gtk.StackSwitcher]: https://docs.gtk.org/gtk4/class.StackSwitcher.html
