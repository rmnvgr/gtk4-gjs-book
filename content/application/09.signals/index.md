---
title: Signals
summary: Make a GTK4+GJS application react to events from widgets
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/signals
weight: 9
slug: signals
---

All this time, the button of our Welcome widget has been sitting there, useless. It's now time to give it a purpose!

## Connecting a Signal to a Callback

Some GTK widgets emit signals in response to user interaction (and in general, GObjects emit signals in response to state change). Our button has a [`clicked` signal][Gtk.Button::clicked], we'll connect to it to run some code in our class.

In UI templates, we can use the `signal` tag, with the signal we want to connect to in the `name` attribute and the name of callback method of our class in `handler`.

So, in `data/ui/WelcomeWidget.ui`, to connect the `clicked` signal of the button to an `onButtonClicked` callback:

```xml {linenostart=24,hl_lines=3}
<object class="GtkButton">
	<!-- ... -->
	<signal name="clicked" handler="onButtonClicked"/>
</object>
```

Of course, we have to define the callback function in our `WelcomeWidget` class in `src/WelcomeWidget.js`:

```js {linenostart=18,hl_lines="4-6"}
class extends Gtk.Widget {
	/* ... */

	onButtonClicked(_button) {
		console.log('Button clicked!');
	}
}
```

The first parameter of a signal callback will always be the object emitting it. Some signals can have additional parameters, the documentation will mention them.

{{% info %}}
It's also possible to connect to signals in code with the [`connect()` function][GObject.Object.connect()]:

```js {linenos=false}
targetObject.connect('signal-name', callbackFunction);
```
{{% /info %}}

If you run the application and click the button, the console should display "Button clicked!".

## Emitting Signals

This is nice, but not really useful, as the widget does nothing with the information. It would be great if the window changed to the Files view. The window, however, has no way of knowing that the button has been pressed.

One solution is for our widget to emit another signal that the window can connect to. (Another solution is to use actions, as we'll see in [the next chapter][Actions].)

We declare a simple signal in the `Signal` object of the configuration object of our class:

```js {linenostart=4,hl_lines="3-5"}
export const WelcomeWidget = GObject.registerClass({
	/* ... */
	Signals: {
		'button-clicked': {},
	},
}, class extends Gtk.Widget {/* ... */}
});
```

{{% info %}}
It's possible to create more complicated signals with specific behavior and additional parameters, please refer to [the GJS documentation][GJS GObject Sublassing#signals] to learn more about it.
{{% /info %}}

Now, instead of logging a message, we emit this signal when we receive the `clicked` signal from the button:

```js {linenostart=35,hl_lines=2}
onButtonClicked(_button) {
	this.emit('button-clicked');
}
```

In `data/ui/Window.ui`, we connect to the `button-clicked` signal of our widget:

```xml {linenostart=20,hl_lines=3}
<object class="FbrWelcomeWidget">
	<!-- ... -->
	<signal name="button-clicked" handler="onWelcomeButtonClicked"/>
</object>
```

And in `src/Window.js`, we implement the `onWelcomeButtonClicked` callback function, that will change the view to the Files view:

```js {linenostart=4,hl_lines=[3,"7-9"]}
export const Window = GObject.registerClass({
	/* ... */
	InternalChildren: ['viewStack'],
}, class extends Gtk.ApplicationWindow {
	/* ... */

	onWelcomeButtonClicked(_widget) {
		this._viewStack.visibleChildName = 'files';
	}
});
```

That's it, now when we run our application and click on the button, the window switches to the Files view!


[Actions]: {{< ref "../10.actions/index.md" >}}

[GJS GObject Sublassing#signals]: https://gjs.guide/guides/gobject/subclassing.html#signals

[GObject.Object.connect()]: https://docs.gtk.org/gobject/method.Object.connect.html

[Gtk.Button::clicked]: https://docs.gtk.org/gtk4/signal.Button.clicked.html
