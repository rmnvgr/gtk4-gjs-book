---
title: Actions
summary: Trigger global actions from any widget in a GTK4 application with GActions
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/actions
weight: 10
slug: actions
---

In the previous chapter, we used a chain of signals from a widget to the top-level window to trigger an action.

However, there's a more convenient solution: [actions][GTK Actions]!

When a widget activates an action, GTK walks up the hierarchy to find a widget implementing the requested action, and run it if it does.

## Adding an Action

Actions can be directly added to a [`Gtk.Application`][Gtk.Application] or a [`Gtk.ApplicationWindow`][Gtk.ApplicationWindow], because they implement the [`Gio.ActionMap` interface][Gio.ActionMap]. Actions added to the application can be activated from any widget in any window, while those added to a window can only be activated by widgets from this window.

Actions can also be added to any widget, for instance by creating a [`Gio.SimpleActionGroup`][Gio.SimpleActionGroup], adding actions to it, and inserting it into the widget with the [`insert_action_group()` function][Gtk.Widget.insert_action_group()].

We'll add an action to our window that will allow us to change the view. This action will have:

- a name, `change-view`, that will get used to activate the action
- a parameter, telling which view we want to display
- a callback function, implementing the actual code

We'll create the action with the [`Gio.SimpleAction` class][Gio.SimpleAction].

Because actions can actually be called from _outside_ the application, the parameter has to be serialized through a [`GLib.Variant`][GLib.Variant]. We'll use a simple string variant, but it's possible to create much more complex variants, refer to the GLib documentation on [format strings][GLib GVariant Format Strings] and [text format][GLib GVariant Text Format] and [the GJS guide][GJS GVariant] to learn more about it. If your action doesn't need a parameter, you can simply pass `null`.

It's time to code! In `data/ui/Window.ui`, remove the `signal` element we added previously, and in `src/Window.js`, remove the `onWelcomeButtonClicked()` callback. Then, in the window constructor, we create the action and add it:

```js
import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
```

```js {linenostart=10,hl_lines=["2-5","9-23"]}
class extends Gtk.ApplicationWindow {
	constructor(params={}) {
		super(params);
		this.#setupActions();
	}

	/* ... */

	#setupActions() {
		// Create the action
		const changeViewAction = new Gio.SimpleAction({
			name: 'change-view',
			parameterType: GLib.VariantType.new('s'),
		});

		// Connect to the activate signal to run the callback
		changeViewAction.connect('activate', (_action, params) => {
			this._viewStack.visibleChildName = params.unpack();
		});

		// Add the action to the window
		this.add_action(changeViewAction);
	}
}
```

As you can see, GJS has a convenient [`unpack()` function][GJS GVariant#Unpacking] to deserialize the variant back to the original value.

## Activating an Action

Now that we created the action, we have to activate it.

We can do so in code with the [`activate_action()` function][Gtk.Widget.activate_action()], but because our button implements the [`Gtk.Actionable` interface][Gtk.Actionable], we can actually define it in the UI template.

We start by removing the `onButtonClicked()` callback in `src/WelcomeWidget.js`, and the `signal` element in `data/ui/WelcomeWidget.ui`. Then we add the `action-name` and `action-target` properties to the button:

```xml {linenostart=24,hl_lines="2-3"}
<object class="GtkButton">
	<property name="action-name">win.change-view</property>
	<property name="action-target">'files'</property>
	<!-- ... -->
</object>
```

We prefix the `action-name` with the name of the action group the action is in. For the application window, the action group is `win`, for the application it is `app`.

The `action-target` is our parameter in [GVariant text format][GLib GVariant Text Format]. Because it's a string, we put it between quotes.

One neat thing to note is that if the action is unavailable, the button will become insensitive.

---

Now, when we run our application, clicking on the button still changes the view to the Files view, but we no longer have a chain of signals from the widget to the window, the action can be called from anywhere!


[GJS GVariant]: https://gjs.guide/guides/glib/gvariant.html
[GJS GVariant#Unpacking]: https://gjs.guide/guides/glib/gvariant.html#unpacking-variants
[GLib GVariant Format Strings]: https://docs.gtk.org/glib/gvariant-format-strings.html
[GLib GVariant Text Format]: https://docs.gtk.org/glib/gvariant-text.html
[GTK Actions]: https://docs.gtk.org/gtk4/actions.html

[Gio.ActionMap]: https://docs.gtk.org/gio/iface.ActionMap.html
[Gio.SimpleAction]: https://docs.gtk.org/gio/class.SimpleAction.html
[Gio.SimpleActionGroup]: https://docs.gtk.org/gio/class.SimpleActionGroup.html

[GLib.Variant]: https://docs.gtk.org/glib/struct.Variant.html

[Gtk.Actionable]: https://docs.gtk.org/gtk4/iface.Actionable.html
[Gtk.Application]: https://docs.gtk.org/gtk4/class.Application.html
[Gtk.ApplicationWindow]: https://docs.gtk.org/gtk4/class.ApplicationWindow.html
[Gtk.Widget.activate_action()]: https://docs.gtk.org/gtk4/method.Widget.activate_action.html
[Gtk.Widget.insert_action_group()]: https://docs.gtk.org/gtk4/method.Widget.insert_action_group.html
