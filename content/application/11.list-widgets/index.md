---
title: Displaying a Lot of Objects With List Widgets
summary: Efficiently display a lot of objects with GTK4 list widgets
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/list-widgets
weight: 11
slug: list-widgets
---

If you deal with a lot of objects and want to display them efficiently, GTK4 introduces [list widgets][GTK List Widgets], that are tailored to that usage. Such widgets include [`Gtk.ListView`][Gtk.ListView] and [`Gtk.GridView`][Gtk.GridView] that work in the same fashion.

They work with _models_, which perform operations on a base [`Gio.ListModel`][Gio.ListModel] that holds the objects. These models can be a [`Gtk.FilterListModel`][Gtk.FilterListModel], that filters the list according to some criteria, or a [`Gtk.SortListModel`][Gtk.SortListModel], that sorts the list. These models can be chained if you need to perform several operations on the list. The list widgets require a [`Gtk.SelectionModel`][Gtk.SelectionModel], which deals with the selection of the underlying objects.

Once the list widgets have the list of items, they transform them into proper widgets through a [`Gtk.ListItemFactory`][Gtk.ListItemFactory]. However, they only create a handful of widgets, and recycle them as they appear into and disappear from the view, changing the properties on the fly.

The list widgets must be placed in a [`Gtk.ScrolledWindow`][Gtk.ScrolledWindow] for the recycling of list items to work.

We'll use the list widgets in our application to display a list of files. Because we don't know how many files a user can have, they are a perfect fit for this.

## Creating a New View

To separate the files-related code from the rest of the application, we'll create a new `FilesView` widget.

In a new `src/FilesView.js` file:

```js
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

export const FilesView = GObject.registerClass({
	GTypeName: 'FbrFilesView',
	Template: 'resource:///org/example/filebrowser/ui/FilesView.ui',
}, class extends Gtk.Widget {});
```

And in a new `data/ui/FilesView.ui`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<template class="FbrFilesView">
		<property name="layout-manager">
			<object class="GtkBinLayout"/>
		</property>
	</template>
</interface>
```

Don't forget to add these two files to their respective resource file.

Import the new widget in `src/Application.js`:

```js {linenostart=5}
import './FilesView.js';
```

And finally, we replace the placeholder label by our new widget in the `files` page of the view stack in `data/ui/Window.ui`:

```xml {linenostart=31,hl_lines=5}
<object class="GtkStackPage">
	<property name="name">files</property>
	<property name="title">Files</property>
	<property name="child">
		<object class="FbrFilesView"/>
	</property>
</object>
```

## Getting a List

To represent the data, we'll create a `FbrFile` GObject, with three properties:

- `name`, the name of the file that we'll display
- `icon`, a [`Gio.Icon`][Gio.Icon] representing the file content type
- `type`, a [`Gio.FileType`][Gio.FileType]

In a new `src/File.js` file (don't forget to add it to the resources):

```js
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';


export const File = GObject.registerClass({
	GTypeName: 'FbrFile',
	Properties: {
		name: GObject.ParamSpec.string('name', 'Name', 'Name of the file', GObject.ParamFlags.READWRITE, ''),
		icon: GObject.ParamSpec.object('icon', 'Icon', 'Icon for the file', GObject.ParamFlags.READWRITE, Gio.Icon),
		type: GObject.ParamSpec.enum('type', 'Type', 'File type', GObject.ParamFlags.READWRITE, Gio.FileType, Gio.FileType.UNKNOWN),
	}
}, class extends GObject.Object {});
```

We'll store the list of files in the `files` property of the `FilesView` class. It will be a [`Gio.ListStore`][Gio.ListStore], which implements the `Gio.ListModel` interface expected by the list widgets:

```js
import Gio from 'gi://Gio';
```

```js {linenostart=5,hl_lines="3-11"}
export const FilesView = GObject.registerClass({
	/* ... */
	Properties: {
		files: GObject.ParamSpec.object(
			'files',
			'Files',
			'The list model containing the files',
			GObject.ParamFlags.READWRITE,
			Gio.ListStore
		),
	},
}, class extends Gtk.Widget {});
```

Now, let's get a list of files from the current directory. For that, we'll use Gio functions to enumerate the files. The GJS Guide has [a chapter dedicated to file operations][GJS File Operations] if you want to learn more.

```js {linenostart=2}
import GLib from 'gi://GLib';
```

```js {linenostart=6}
import { File } from './File.js';
```

```js {linenostart=21, hl_lines="2-26"}
class extends Gtk.Widget {
	constructor(params={}) {
		super(params);
		this.#initializeFiles();
	}

	#initializeFiles() {
		// Create the Gio.ListStore that will contain File objects
		this.files = Gio.ListStore.new(File);

		// Get the current directory
		const currentDir = Gio.File.new_for_path(GLib.get_current_dir());

		// Get an enumerator of all children
		const children = currentDir.enumerate_children('standard::*', Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS, null);

		// Iterate over the enumerator and add each child to the list store
		let fileInfo;
		while (fileInfo = children.next_file(null)) {
			this.files.append(new File({
				name: fileInfo.get_display_name(),
				icon: Gio.content_type_get_icon(fileInfo.get_content_type()),
				type: fileInfo.get_file_type(),
			}));
		}
	}
}
```

{{% info %}}
For the sake of simplicity, we're using synchronous, blocking functions. In an actual application, use their asynchronous counterparts! The [GJS Guide][GJS File Operations] has plenty of examples.
{{% /info %}}

Because we're in a sandbox environment, the application can't actually access the files. We have to add read-only access to the `home` directory in the Flatpak manifest in `org.example.filebrowser.yml`:

```yml {linenostart=8,hl_lines=3}
finish-args:
  # ...
  - --filesystem=home:ro
```

## Displaying Items

Now that we have our list, we need to display each item. Let's do this step by step!

### Adding a List View

The `Gtk.ListView` widget that will display our items must be the child of a `Gtk.ScrolledWindow`. So in `data/ui/FilesView.ui`, let's add a scrolled window as the child of the widget, and a list view as the child of this scrolled window:

```xml {linenostart=3,hl_lines="3-10"}
<template class="FilesView">
	<!-- ...-->
	<child>
		<object class="GtkScrolledWindow">
			<property name="child">
				<object class="GtkListView">
				</object>
			</property>
		</object>
	</child>
</template>
```

### Setting Up the Model

The model of the list view must be a `Gtk.SelectionModel`. Since we want our users to be able to select multiple files, we'll use a [`Gtk.MultiSelection` model][Gtk.MultiSelection]. We'll also set the `enable-rubberband` property to let users select multiple files with their mouse.

```xml {linenostart=10,hl_lines="2-6"}
<object class="GtkListView">
	<property name="enable-rubberband">true</property>
	<property name="model">
		<object class="GtkMultiSelection">
		</object>
	</property>
</object>
```

### Setting Up the Model of the Selection Model

We have to feed the selection model with our actual data. Since we store our list of files in the `files` property, we can simply use property binding:

```xml {linenostart=13,hl_lines=2}
<object class="GtkMultiSelection">
	<property name="model" bind-source="FbrFilesView" bind-property="files"/>
</object>
```

### Setting Up the Factory

Now, to transform our data into a widget, we have to use a `Gtk.ListItemFactory`. Because we'll write it in a UI template, we'll use a [`Gtk.BuilderListItemFactory`][Gtk.BuilderListItemFactory], that will load a template from the resources:

```xml {linenostart=10,hl_lines="3-7"}
<object class="GtkListView">
	<!-- ... -->
	<property name="factory">
		<object class="GtkBuilderListItemFactory">
			<property name="resource">/org/example/filebrowser/ui/FileListItem.ui</property>
		</object>
	</property>
</object>
```

### Creating the Template

Of course, this template doesn't yet exist. Create a `data/ui/FileListItem.ui` (don't forget to add it to the list of resources), and put this inside:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<template class="GtkListItem">
		<property name="child">
		</property>
	</template>
</interface>
```

The template must be of class `GtkListItem` so that the factory knows what to do with it. The child widget of the list item will be added as a row of the list view.

Let's display the icon and the name of the file by putting a `Gtk.Image` and a `Gtk.Label` inside a `Gtk.Box`, itself a child of the `Gtk.ListItem`:

```xml {linenostart=3,hl_lines="3-14"}
<template class="GtkListItem">
	<property name="child">
		<object class="GtkBox">
			<property name="spacing">6</property>
			<child>
				<object class="GtkImage">
				</object>
			</child>
			<child>
				<object class="GtkLabel">
					<property name="xalign">0</property>
				</object>
			</child>
		</object>
	</property>
</template>
```

There's just one problem: how do we get the icon and name of the item?

### Accessing the Item in the Template With Expressions

Well, the [`Gtk.ListItem` object][Gtk.ListItem] doesn't only have a `child` property: it also has an [`item` property][Gtk.ListItem:item], which holds our object.

However, to access its properties, we can't use property bindings, as it can't go deeper than one level and only gives us access to `item`, and we need to access `item.icon` and `item.name`. Fortunately, there's a more powerful tool at our disposal: [`Gtk.Expression`][Gtk.Expression]. We're specifically interested in the [`Gtk.PropertyExpression`][Gtk.PropertyExpression], that allows us to access multiple levels of properties!

To use it in a UI template, we start with a `binding` element, with its `name` attribute containing the property we want to bind in the widget. Then, each `lookup` child element will tell which property `name` to lookup in its own `lookup` child of a given `type`, the deepest `lookup` element being set to the object we want to lookup.

That sounds complicated, but translated in code, it's much easier to understand.

For the image:

```xml {linenostart=8,hl_lines="2-6"}
<object class="GtkImage">
	<binding name="gicon">
		<lookup name="icon" type="FbrFile">
			<lookup name="item">GtkListItem</lookup>
		</lookup>
	</binding>
</object>
```

And for the label:

```xml {linenostart=17,hl_lines="2-6"}
<object class="GtkLabel">
	<binding name="label">
		<lookup name="name" type="FbrFile">
			<lookup name="item">GtkListItem</lookup>
		</lookup>
	</binding>
	<!-- ... -->
</object>
```

Now, if we run our application and go to the Files view, we can see that it displays a list of files! It finally deserves its name!

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window-1.png"
	src-2x="screenshots/window-1-2x.png"
	src-dark="screenshots/window-1-dark.png"
	src-dark-2x="screenshots/window-1-dark-2x.png"
>}}
<!-- spellchecker-enable -->

But we can do more with the models!

## Sorting Items

You may have noticed that files are in a random order. Let's use a [`Gtk.SortListModel`][Gtk.SortListModel] to fix that.

We have to insert our `Gtk.SortListModel` between the selection model and the list model in `data/ui/FilesView.ui`:

```xml {linenostart=13,hl_lines=["2-3","5-6"]}
<object class="GtkMultiSelection">
	<property name="model">
		<object class="GtkSortListModel">
			<property name="model" bind-source="FbrFilesView" bind-property="files"/>
		</object>
	</property>
</object>
```

This model uses a [`Gtk.Sorter`][Gtk.Sorter] to sort the items. Since we want to sort names, we'll use a [`Gtk.StringSorter`][Gtk.StringSorter]. It takes a `Gtk.Expression` to know which property of the item to compare.

So we lookup the `name` property of the `FbrFile` object:

```xml {linenostart=15,hl_lines="3-9"}
<object class="GtkSortListModel">
	<!-- ... -->
	<property name="sorter">
		<object class="GtkStringSorter">
			<property name="expression">
				<lookup name="name" type="FbrFile"/>
			</property>
		</object>
	</property>
</object>
```

And now, our files are sorted by name!

{{% info %}}
The items are only sorted in this model, and other models that consume it. Items in the list store are still in their original order!
{{% /info %}}

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window-2.png"
	src-2x="screenshots/window-2-2x.png"
	src-dark="screenshots/window-2-dark.png"
	src-dark-2x="screenshots/window-2-dark-2x.png"
>}}
<!-- spellchecker-enable -->

{{% info %}}
As you can see, the sorter doesn't care if items are a file or a directory. You can use a [`Gtk.CustomSorter`][Gtk.CustomSorter] to have more control over the sorting.
{{% /info %}}

## Filtering Items

If you run the application in a directory that contains hidden files, they will appear in the view. What if we don't want to see them? `Gtk.FilterListModel` comes to the rescue!

### Creating the Filter

We insert it between the selection model and the sort model:

```xml {linenostart=13,hl_lines=["3-4","8-9"]}
<object class="GtkMultiSelection">
	<property name="model">
		<object class="GtkFilterListModel" id="hiddenFileFilterModel">
			<property name="model">
				<object class="GtkSortListModel">
					<!-- ... -->
				</object>
			</property>
		</object>
	</property>
</object>
```

We gave it an ID because we'll need to access it from code later. So, in `src/FilesView.js`, in the configuration object:

```js {linenostart=9,hl_lines=3}
export const FilesView = GObject.registerClass({
	/* ... */
	InternalChildren: ['hiddenFileFilterModel'],
}, class extends Gtk.Widget {/* ... */});
```

Its `filter` property takes a [`Gtk.Filter`][Gtk.Filter] to tell it how to filter items. GTK provides several filters, but since we want to perform a particular filter, we'll use a [`Gtk.CustomFilter`][Gtk.CustomFilter]. This filter needs a function that will return either `true` if the item should be displayed or `false` if it should be filtered.

We create the filter when the object is constructed. If the file name starts with a `.`, it is a hidden file, and we filter it:

```js {linenostart=22,hl_lines=[5,"8-10"]}
class extends Gtk.Widget {
	constructor(params={}) {
		super(params);
		/* ... */
		this.#setupHiddenFileFilterModel();
	}

	#setupHiddenFileFilterModel(){
		this._hiddenFileFilterModel.filter = Gtk.CustomFilter.new(item => item.name.charAt(0) !== '.');
	}
}
```

And now all the hidden files are filtered!

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window-3.png"
	src-2x="screenshots/window-3-2x.png"
	src-dark="screenshots/window-3-dark.png"
	src-dark-2x="screenshots/window-3-dark-2x.png"
>}}
<!-- spellchecker-enable -->

But what if we want to give our users the option to display them? Well, filters can be changed during runtime!

### Updating the Filter

Let's add a [`Gtk.CheckButton`][Gtk.CheckButton] in our Files view and connect to its `toggled` signal:

```xml {linenostart=3,hl_lines=["3-5","7-12",15]}
<template class="FbrFilesView">
	<property name="layout-manager">
		<object class="GtkBoxLayout">
			<property name="orientation">vertical</property>
		</object>
	</property>
	<child>
		<object class="GtkCheckButton">
			<property name="label">Show hidden files</property>
			<signal name="toggled" handler="onShowHiddenFilesButtonToggled"/>
		</object>
	</child>
	<child>
		<object class="GtkScrolledWindow">
			<property name="vexpand">true</property>
			<!-- ... -->
		</object>
	</child>
</template>
```

In the callback function, we call the `changed` method on the filter. It takes a [`Gtk.FilterChange`][Gtk.FilterChange], telling the filter if more or less items will be filtered so that it can optimize the process. We also store the state of the button in order to use it in the filter function.

```js {linenostart=22,hl_lines=[2,"6-13"]}
class extends Gtk.Widget {
	#showHiddenFiles = false;

	/* ... */

	onShowHiddenFilesButtonToggled(button) {
		this.#showHiddenFiles = button.active;
		this._hiddenFileFilterModel.filter.changed(
			button.active
			? Gtk.FilterChange.LESS_STRICT
			: Gtk.FilterChange.MORE_STRICT
		);
	}
}
```

And we update the filter function so that it only filters hidden files if the button is active:

```js {linenostart=52,hl_lines="2-4"}
#setupHiddenFileFilterModel(){
	this._hiddenFileFilterModel.filter = Gtk.CustomFilter.new(item => {
		return this.#showHiddenFiles ? true : item.name.charAt(0) !== '.';
	});
}
```

Now, run the application and toggle the button: hidden files appear and disappear at will!

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window-4.png"
	src-2x="screenshots/window-4-2x.png"
	src-dark="screenshots/window-4-dark.png"
	src-dark-2x="screenshots/window-4-dark-2x.png"
>}}
<!-- spellchecker-enable -->

## List View Signals

When the user interacts with the list view, it's possible to respond to various signals.

### List Activation

If a row is activated (by default by double-clicking it), the list view will emit the [`activate` signal][Gtk.ListView::activate]. Let's connect to it:

```xml {linenostart=19,hl_lines=3}
<object class="GtkListView">
	<!-- ... -->
	<signal name="activate" handler="onListViewActivated"/>
</object>
```

The signal has the position of the activated item as a parameter, so it's easy to retrieve it in the callback function:

```js {linenostart=67}
onListViewActivated(listview, position) {
	console.log('Row activated!');
	const item = listview.model.get_item(position);
	console.log(item.name);
}
```

Note that we use the model of the list view to retrieve the item and not our files model, since the model can be filtered, and the positions won't match.

### Selection Changed

When the user selects or deselects items, the selection model emits the [`selection-changed` signal][Gtk.SelectionModel::selection-changed]. Let's connect to it:

```xml {linenostart=22,hl_lines=3}
<object class="GtkMultiSelection">
	<!-- ... -->
	<signal name="selection-changed" handler="onSelectionChanged"/>
</object>
```

And retrieve the items in the callback function:

```js {linenostart=73}
onSelectionChanged(selectionmodel, _position, _n_items) {
	console.log('Selection changed!');
	const selection = selectionmodel.get_selection();
	for (let i = 0; i < selection.get_size(); i++) {
		const position = selection.get_nth(i);
		const item = selectionmodel.get_item(position);
		console.log(item.name);
	}
}
```

We don't use the `position` and `n_items` parameters as the selection can grow or shrink, and not be continuous, so we get the selection from the selection model and iterate over the positions.

---

This chapter was pretty intense, but list widgets are really powerful if you need to display a lot of objects. We only explored the `Gtk.ListView` widget, but experiment with the other list widgets, they work in the same fashion!

One neat thing with list widgets and models is that if you add or remove an item in the list, the view will be automatically updated.


[GJS File Operations]: https://gjs.guide/guides/gio/file-operations.html
[GTK List Widgets]: https://docs.gtk.org/gtk4/section-list-widget.html

[Gio.FileType]: https://docs.gtk.org/gio/enum.FileType.html
[Gio.Icon]: https://docs.gtk.org/gio/iface.Icon.html
[Gio.ListModel]: https://docs.gtk.org/gio/iface.ListModel.html
[Gio.ListStore]: https://docs.gtk.org/gio/class.ListStore.html

[Gtk.BuilderListItemFactory]: https://docs.gtk.org/gtk4/class.BuilderListItemFactory.html
[Gtk.CheckButton]: https://docs.gtk.org/gtk4/class.CheckButton.html
[Gtk.CustomFilter]: https://docs.gtk.org/gtk4/class.CustomFilter.html
[Gtk.CustomSorter]: https://docs.gtk.org/gtk4/class.CustomSorter.html
[Gtk.Expression]: https://docs.gtk.org/gtk4/class.Expression.html
[Gtk.Filter]: https://docs.gtk.org/gtk4/class.Filter.html
[Gtk.FilterChange]: https://docs.gtk.org/gtk4/enum.FilterChange.html
[Gtk.FilterListModel]: https://docs.gtk.org/gtk4/class.FilterListModel.html
[Gtk.GridView]: https://docs.gtk.org/gtk4/class.GridView.html
[Gtk.ListItem]: https://docs.gtk.org/gtk4/class.ListItem.html
[Gtk.ListItem:item]: https://docs.gtk.org/gtk4/property.ListItem.item.html
[Gtk.ListItemFactory]: https://docs.gtk.org/gtk4/class.ListItemFactory.html
[Gtk.ListView]: https://docs.gtk.org/gtk4/class.ListView.html
[Gtk.ListView::activate]: https://docs.gtk.org/gtk4/signal.ListView.activate.html
[Gtk.MultiSelection]: https://docs.gtk.org/gtk4/class.MultiSelection.html
[Gtk.PropertyExpression]: https://docs.gtk.org/gtk4/class.PropertyExpression.html
[Gtk.ScrolledWindow]: https://docs.gtk.org/gtk4/class.ScrolledWindow.html
[Gtk.SelectionModel]: https://docs.gtk.org/gtk4/iface.SelectionModel.html
[Gtk.SelectionModel::selection-changed]: https://docs.gtk.org/gtk4/signal.SelectionModel.selection-changed.html
[Gtk.Sorter]: https://docs.gtk.org/gtk4/class.Sorter.html
[Gtk.SortListModel]: https://docs.gtk.org/gtk4/class.SortListModel.html
[Gtk.StringSorter]: https://docs.gtk.org/gtk4/class.StringSorter.html
