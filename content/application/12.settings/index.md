---
title: Storing and Loading Settings
summary: Store and load settings in a GTK4 application with GSettings
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/settings
weight: 12
slug: settings
---

The Files view of our application has a button to show hidden files. However, its state is not saved when the application is closed. Same if the user resizes the window, its dimensions won't be remembered. That can be easily done with [`Gio.Settings`][Gio.Settings]!

{{% info %}}
Use `Gio.Settings` to store application settings, not user data!
{{% /info %}}

## Defining a Schema

`Gio.Settings` needs our settings to be clearly defined in a schema, written in XML.

The root element must be `schemalist`, with a child `schema` element having an `id` attribute with the application ID and a `path` attribute similar to the path used in resources. The `schema` element has `key` children, each with `name` and `type` attributes, giving the name and [format string][GLib GVariant Format Strings] of the setting, and with a child `default` element giving the default value. You can read the [documentation page][Gio.Settings] to get an overview of all the other possible elements, especially if you want to store complex values.

In our settings, we want to store three values: the window's width and height, which is an integer, and if hidden files should be shown, which is a boolean. So, in a new `data/org.example.filebrowser.gschema.xml` file, we declare this:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<schemalist>
	<schema id="org.example.filebrowser" path="/org/example/filebrowser/">
		<key name="window-width" type="i">
			<default>-1</default>
		</key>
		<key name="window-height" type="i">
			<default>-1</default>
		</key>
		<key name="show-hidden-files" type="b">
			<default>false</default>
		</key>
	</schema>
</schemalist>
```

In order to be used, the schema must be installed and compiled. Luckily, our build system has a built-in module allowing us to do that. The schema must be placed in the `$DATADIR/glib-2.0/schemas` directory.

In `data/meson.build`, we install the schema:

```meson {linenostart=10}
# Install the settings schema
install_data(
	APP_ID + '.gschema.xml',
	install_dir: get_option('datadir') / 'glib-2.0' / 'schemas'
)
```

Now, back in `meson.build`, we tell the build system to compile the schema after installation:

```meson {linenostart=32}
# Post installation tasks
gnome.post_install(
	glib_compile_schemas: true
)
```

We're ready to use our settings!

## Loading the Settings

We'll load our settings in the application startup, and make them available globally.

In `src/Application.js`:

```js {linenostart=2}
import Gio from 'gi://Gio';
```

```js {linenostart=12,hl_lines=[4,"9-12"]}
class extends Gtk.Application {
	vfunc_startup() {
		/* ... */
		this.#loadSettings();
	}

	/* ... */

	#loadSettings() {
		// Load the settings
		globalThis.settings = new Gio.Settings({ schemaId: this.applicationId });
	}
}
```

We could now retrieve the values with all the `get_*()` methods of `Gio.Settings`, but there are more powerful alternatives available!

## Binding Properties to Settings

It's possible to bind object properties to settings values with the [`Gio.Settings.bind()` function][Gio.Settings.bind()]: with the default [`Gio.SettingsBindFlags`][Gio.SettingsBindFlags], when the property changes, the value is saved in the settings, and when the value changes in the settings, the property is updated.

Let's use this in `src/Window.js` to bind the window's [`default-width`][Gtk.Window:default-width] and [`default-height`][Gtk.Window:default-height] properties to the `window-width` and `window-height` settings keys:

```js {linenostart=10,hl_lines=[4,"7-11"]}
class extends Gtk.ApplicationWindow {
	constructor(params={}) {
		/* ... */
		this.#bindSizeToSettings();
	}

	#bindSizeToSettings() {
		// Bind the width and height to the settings
		settings.bind('window-width', this, 'default-width', Gio.SettingsBindFlags.DEFAULT);
		settings.bind('window-height', this, 'default-height', Gio.SettingsBindFlags.DEFAULT);
	}
}
```

Because we assigned the settings to `globalThis.settings`, we can access `settings` globally.

Try resizing the window and restart the application: the previous size should be restored!

## Binding Actions to Settings

We could also bind the `show-hidden-files` setting to our button in the Files view. We can however do something a little more interesting: create a settings action. Because we're using a boolean, the action's state will hold the value.

We start by creating the action in `src/Application.js` with the [`Gio.Settings.create_action()` function][Gio.Settings.create_action()] and adding it to the application:

```js {linenostart=12,hl_lines=[4,"7-10"]}
class extends Gtk.Application {
	vfunc_startup() {
		/* ... */
		this.#setupActions();
	}

	#setupActions() {
		// Create the show-hidden-files action
		this.add_action(settings.create_action('show-hidden-files'));
	}
}
```

Now, in `data/ui/FilesView.xml`, we remove the `signal` element of the button and we set its `action-name` property to `app.show-hidden-files`, since the action was added to the application and takes the name of the settings key:

```xml {linenostart=10,hl_lines=3}
<object class="GtkCheckButton">
	<property name="label">Show hidden files</property>
	<property name="action-name">app.show-hidden-files</property>
</object>
```

In `src/FilesView.js`, we can remove the `#showHiddenFiles` flags we were using, as well as the `onShowHiddenFilesButtonToggled()` callback function, and use the settings when we setup the custom filter:

```js {linenostart=50,hl_lines=3}
#setupHiddenFileFilterModel(){
	this._hiddenFileFilterModel.filter = Gtk.CustomFilter.new(item => {
		return settings.get_boolean('show-hidden-files') ? true : item.name.charAt(0) !== '.';
	});
}
```

## Connecting to a Key

However, now, the filter is not updated when the value changes. To fix that, we'll connect to the [`changed` signal][Gio.Settings::changed] of the settings.

This signal is special: it accepts a detailed connection, meaning we can be notified only when a specific key changed, by connecting to `changed::key-name`.

So when the widget is constructed, we connect to `changed::show-hidden-files` and update the filter in the callback:

```js {linenostart=22,hl_lines=[4,"9-17"]}
class extends Gtk.Widget {
	constructor(params={}) {
		/* ... */
		this.#connectToSettings();
	}

	/* ... */

	#connectToSettings() {
		settings.connect('changed::show-hidden-files', settings => {
			this._hiddenFileFilterModel.filter.changed(
				settings.get_boolean('show-hidden-files')
				? Gtk.FilterChange.LESS_STRICT
				: Gtk.FilterChange.MORE_STRICT
			);
		});
	}
}
```

And that's it, the filter is now updated whenever the value changes!


[GLib GVariant Format Strings]: https://docs.gtk.org/glib/gvariant-format-strings.html

[Gio.Settings]: https://docs.gtk.org/gio/class.Settings.html
[Gio.Settings.bind()]: https://docs.gtk.org/gio/method.Settings.bind.html
[Gio.Settings.create_action()]: https://docs.gtk.org/gio/method.Settings.create_action.html
[Gio.Settings::changed]: https://docs.gtk.org/gio/signal.Settings.changed.html
[Gio.SettingsBindFlags]: https://docs.gtk.org/gio/flags.SettingsBindFlags.html

[Gtk.Window:default-height]: https://docs.gtk.org/gtk4/property.Window.default-height.html
[Gtk.Window:default-width]: https://docs.gtk.org/gtk4/property.Window.default-width.html
