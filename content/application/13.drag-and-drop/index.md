---
title: Drag and Drop
summary: Easily drag and drop objects inside and outside of a GTK4 application
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/drag-and-drop
weight: 13
slug: drag-and-drop
---

Drag and drop is an interaction that let the user visually move objects, be it inside the application or from/to an outside application.

GTK4 allows to easily implement it thanks to [event controllers][Gtk.EventController]. When added to a widget (either with the [`add_controller()` function][Gtk.Widget.add_controller()] or as a child in a UI template), they then emit signals reacting to user actions.

Let's say we want to allow the user to move a file to another directory with drag and drop. To add that to our application, we'll use the [`Gtk.DragSource`][Gtk.DragSource] and [`Gtk.DropTarget`][Gtk.DropTarget] event controllers.

## Preparing the Widget

We'll start by moving the widgets representing each file to their own widget.

Create the `src/FileRow.js` (and add it to the resources) and put this inside:

```js
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import { File } from './File.js';


export const FileRow = GObject.registerClass({
	GTypeName: 'FbrFileRow',
	Template: 'resource:///org/example/filebrowser/ui/FileRow.ui',
	Properties: {
		file: GObject.ParamSpec.object(
			'file',
			'File',
			'File displayed by the row',
			GObject.ParamFlags.READWRITE,
			File
		),
	},
}, class extends Gtk.Widget {});
```

And in `data/ui/FileRow.ui` (again, add it to the resources):

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<template class="FbrFileRow">
		<property name="layout-manager">
			<object class="GtkBoxLayout">
				<property name="spacing">6</property>
			</object>
		</property>
		<child>
			<object class="GtkImage">
				<binding name="gicon">
					<lookup name="icon" type="FbrFile">
						<lookup name="file">FbrFileRow</lookup>
					</lookup>
				</binding>
			</object>
		</child>
		<child>
			<object class="GtkLabel">
				<binding name="label">
					<lookup name="name" type="FbrFile">
						<lookup name="file">FbrFileRow</lookup>
					</lookup>
				</binding>
				<property name="xalign">0</property>
			</object>
		</child>
	</template>
</interface>
```

We simply moved the widgets inside the box to a widget with a box layout, and replaced the reference to `GtkListItem` with `FileRow` since this is now where the file is stored.

Now, we load our new widget in GObject in `src/Application.js`:

```js {linenostart=6}
import './FileRow.js';
```

And we use it in `data/ui/FileListItem.ui` as the child of the list item:

```xml {linenostart=3,hl_lines="3-8"}
<template class="GtkListItem">
	<property name="child">
		<object class="FbrFileRow">
			<binding name="file">
				<lookup name="item">GtkListItem</lookup>
			</binding>
			<property name="halign">start</property>
		</object>
	</property>
</template>
```

We bind the `file` property of the row with the `item` of the list item, and we align the widget to the start, so that the empty space afterwards doesn't trigger the drag event.

## Setting Up a Drag Source

### Adding a Drag Source

Adding a drag source to a widget is as simple as adding as a child in the UI template!

In `data/ui/FileRow.ui`:

```xml {linenostart=3,hl_lines="3-7"}
<template class="FbrFileRow">
	<!-- ... -->
	<child>
		<object class="GtkDragSource">
			<property name="actions">move</property>
		</object>
	</child>
</template>
```

Since we want to move files, we set the [`actions` property][Gtk.DragSource:actions] to the [`move` drag action][Gtk.DragAction]. This is only an indication for the drop target, it won't actually move anything until we code it.

### Preparing the Content

To tell GTK what we want to drag, we do so in the callback function of the [`prepare` signal][Gtk.DragSource::prepare]:

```xml {linenostart=29,hl_lines=3}
<object class="GtkDragSource">
	<!-- ... -->
	<signal name="prepare" handler="onDragPrepare"/>
</object>
```

It must return a [`Gdk.ContentProvider`][Gdk.ContentProvider] containing our object. We can't add it directly though, we have to store it in a [`GObject.Value`][GObject.Value].

In `src/FileRow.js`:

```js
import Gdk from 'gi://Gdk';
```

```js {linenostart=20,hl_lines="2-11"}
class extends Gtk.Widget {
	onDragPrepare(source, _x, _y) {
		// Create the value
		const value = new GObject.Value();
		// Initialize the value with the object type it will hold
		value.init(File);
		// Put the object in the value
		value.set_object(this.file);
		// Return the content provider for this value
		return Gdk.ContentProvider.new_for_value(value);
	}
}
```

### Adding an Icon

To improve the user experience, we can add an icon that will follow the cursor while dragging with the [`set_icon()` function][Gtk.DragSource.set_icon()]. We can simply display the row with a [`Gtk.WidgetPaintable`][Gtk.WidgetPaintable]:

```js {linenostart=21,hl_lines="2-3"}
onDragPrepare(source, _x, _y) {
	// Set the icon to the widget
	source.set_icon(new Gtk.WidgetPaintable({ widget: this }), 0, 0);

	/* ... */
}
```

### Reacting to a Drag

Right now, if you try to drag a file, it will interfere with the rubber band selection of the list view. To fix that, we can disable it when the drag begins, and enable it again when it ends, thanks to the [`drag-begin`][Gtk.DragSource::drag-begin] and [`drag-end`][Gtk.DragSource::drag-end] signals.

```xml {linenostart=29,hl_lines="3-4"}
<object class="GtkDragSource">
	<!-- ... -->
	<signal name="drag-begin" handler="onDragBegin"/>
	<signal name="drag-end" handler="onDragEnd"/>
</object>
```

In `src/FilesView.js`, we add a [property action][Gio.PropertyAction] whose state will toggle the `dragging` property accordingly:

```js {linenostart=12,hl_lines="3-9"}
Properties: {
	/* ... */
	dragging: GObject.ParamSpec.boolean(
		'dragging',
		'Dragging',
		'If there is dragging happening in the view',
		GObject.ParamFlags.READWRITE,
		false
	),
},
```

```js {linenostart=29,hl_lines=[4,"9-20"]}
class extends Gtk.Widget {
	constructor(params={}) {
		/* ... */
		this.#setupActions();
	}

	/* ... */

	#setupActions() {
		const actionGroup = new Gio.SimpleActionGroup();

		const dragAction = new Gio.PropertyAction({
			name: 'drag',
			propertyName: 'dragging',
			object: this,
		});
		actionGroup.insert(dragAction);

		this.insert_action_group('files-view', actionGroup);
	}
}
```

And we bind it to the list view `enable-rubberband` property in `data/ui/FilesView.ui`:

```xml {linenostart=19,hl_lines=2}
<object class="GtkListView">
	<property name="enable-rubberband" bind-source="FbrFilesView" bind-property="dragging" bind-flags="invert-boolean"/>
	<!-- ... -->
</object>
```

Now, back in `src/FileRow.js`, we trigger the action when the drag begins and ends:

```js {linenostart=36}
onDragBegin(_source, _drag) {
	this.activate_action('files-view.drag', null);
}

onDragEnd(_source, _drag, _deleteData) {
	this.activate_action('files-view.drag', null);
}
```

We can now drag our file. But we have nowhere to drop it!

## Setting Up a Drop Target

### Adding a Drop Target

Adding a drop target is similar to adding a drag source.

In `data/ui/FileRow.ui`:

```xml {linenostart=36}
<child>
	<object class="GtkDropTarget">
		<property name="actions">move</property>
		<property name="formats">FbrFile</property>
	</object>
</child>
```

The [`actions` property][Gtk.DropTarget:actions] tell which actions are supported by the drop target, and the [`formats` property][Gtk.DropTarget:formats] which types are accepted.

### Reacting to a Drop

We get the dropped value in the callback function of the [`drop` signal][Gtk.DropTarget::drop].

```xml {linenostart=37,hl_lines=3}
<object class="GtkDropTarget">
	<!-- ... -->
	<signal name="drop" handler="onDrop"/>
</object>
```

```js {linenostart=43}
onDrop(_target, value, _x, _y) {
	console.log(`Move '${value.name}' to '${this.file.name}'`);
	return true;
}
```

GJS unpacks for us the value back to the original object, so we can use it directly.

Try dragging and dropping files, the console should print the origin and destination!

### Accepting or Rejecting a Drop

Drag and drop now works, but there's a slight problem: we can drop files even on non-directories! To fix that, we can react to the [`accept` signal][Gtk.DropTarget::accept], whose callback will return if the drop is accepted or not.

Because we override the default handler, on top of checking if the current file is a directory, we need to check that the candidate to drop is of the right type.

```xml {linenostart=37,hl_lines=3}
<object class="GtkDropTarget">
	<!-- ... -->
	<signal name="accept" handler="onDropAccept"/>
</object>
```

```js {linenostart=2}
import Gio from 'gi://Gio';
```

```js {linenostart=49}
onDropAccept(_target, drop) {
	return this.file.type === Gio.FileType.DIRECTORY && drop.formats.contain_gtype(File);
}
```

## Dragging Objects to an Outside Application

Dragging and dropping objects within our application is nice, but what about dragging objects to an outside application? GTK handles that for us: we only have to add to the drag source a content provider that will be understood by the other application.

Of course, the `FbrFile` object we use is internal to our application, but we can use the more standard [`Gio.File` interface][Gio.File] that GTK will translate for us to other applications.

In `src/File.js`, we add a `gfile` property to store the associated `Gio.File`:

```js {linenostart=7,hl_lines=3}
Properties: {
	/* ... */
	gfile: GObject.ParamSpec.object('gfile', 'GFile', 'GFile of the file', GObject.ParamFlags.READWRITE, Gio.File),
}
```

And we set it when we create the File object in `src/FilesView.js`:

```js {linenostart=51,hl_lines=3}
new File({
	/* ... */
	gfile: children.get_child(fileInfo),
})
```

Now, in `src/FileRow.js`, when preparing the drag, we create a second content provider with the `Gio.File`, and we return a new one which is the union of the original provider and the new one:

```js {linenostart=22,hl_lines="4-16"}
onDragPrepare(source, _x, _y) {
	/* ... */

	const providers = [];
	[[File, this.file], [Gio.File, this.file.gfile]].forEach(([gtype, object]) => {
		// Create the value
		const value = new GObject.Value();
		// Initialize the value with the object type it will hold
		value.init(gtype);
		// Put the object in the value
		value.set_object(object);
		// Add the content provider for this value to the providers
		providers.push(Gdk.ContentProvider.new_for_value(value));
	});
	// Return the union of all the providers
	return Gdk.ContentProvider.new_union(providers);
}
```

Now, when you drag a file from our application to another application that handles files, it will work!

## Accepting Objects From an Outside Application

What about doing it the other way, and accepting `Gio.File`?

That's easy, in `data/ui/FileRow.ui`, we add it to the list of formats accepted by the drop target:

```xml {linenostart=37,hl_lines=3}
<object class="GtkDropTarget">
	<!-- ... -->
	<property name="formats">FbrFile GFile</property>
	<!-- ... -->
</object>
```

{{% info %}}
The prefix for the GLib, Gio and GObject libraries is just `G`.
{{% /info %}}

In the callback to the `accept` signal, we have to allow this type:

```js {linenostart=54,hl_lines="2-3"}
onDropAccept(_target, drop) {
	return this.file.type === Gio.FileType.DIRECTORY &&
		(drop.formats.contain_gtype(File) || drop.formats.contain_gtype(Gio.File));
}
```

And when the data is dropped, we have to handle the value differently depending on its type:

```js {linenostart=49,hl_lines="2-5"}
onDrop(_target, value, _x, _y) {
	if (value instanceof File)
		console.log(`Move '${value.name}' to '${this.file.name}'`);
	else if (value instanceof Gio.File)
		console.log(`Move '${value.get_basename()}' to '${this.file.name}'`);
	return true;
}
```

Now, drag a file from another application, the console should print the origin and destination!


[Gdk.ContentProvider]: https://docs.gtk.org/gdk4/class.ContentProvider.html

[Gio.File]: https://docs.gtk.org/gio/iface.File.html
[Gio.PropertyAction]: https://docs.gtk.org/gio/class.PropertyAction.html

[GObject.Value]: https://docs.gtk.org/gobject/struct.Value.html

[Gtk.DragAction]: https://docs.gtk.org/gdk4/flags.DragAction.html
[Gtk.DragSource]: https://docs.gtk.org/gtk4/class.DragSource.html
[Gtk.DragSource.set_icon()]: https://docs.gtk.org/gtk4/method.DragSource.set_icon.html
[Gtk.DragSource:actions]: https://docs.gtk.org/gtk4/property.DragSource.actions.html
[Gtk.DragSource::drag-begin]: https://docs.gtk.org/gtk4/signal.DragSource.drag-begin.html
[Gtk.DragSource::drag-end]: https://docs.gtk.org/gtk4/signal.DragSource.drag-end.html
[Gtk.DragSource::prepare]: https://docs.gtk.org/gtk4/signal.DragSource.prepare.html
[Gtk.DropTarget]: https://docs.gtk.org/gtk4/class.DropTarget.html
[Gtk.DropTarget:actions]: https://docs.gtk.org/gtk4/property.DropTarget.actions.html
[Gtk.DropTarget:formats]: https://docs.gtk.org/gtk4/property.DropTarget.formats.html
[Gtk.DropTarget::accept]: https://docs.gtk.org/gtk4/signal.DropTarget.accept.html
[Gtk.DropTarget::drop]: https://docs.gtk.org/gtk4/signal.DropTarget.drop.html
[Gtk.EventController]: https://docs.gtk.org/gtk4/class.EventController.html
[Gtk.Widget.add_controller()]: https://docs.gtk.org/gtk4/method.Widget.add_controller.html
[Gtk.WidgetPaintable]: https://docs.gtk.org/gtk4/class.WidgetPaintable.html
