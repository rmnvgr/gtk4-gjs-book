---
title: Keyboard Shortcuts
summary: Let users quickly execute actions with keyboard shortcuts and accelerators
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/keyboard-shortcuts
weight: 14
slug: keyboard-shortcuts
---

Keyboard shortcuts allow users to quickly execute actions. GTK provides us with different mechanisms to add them to our application.

## Adding Accelerators to an Action

The easiest way to add keyboard shortcuts is to add accelerators to global actions (added to the application or the window), thanks to the [`set_accels_for_action()` method][Gtk.Application.set_accels_for_action()] of `Gtk.Application`. The syntax for the accelerators can be found on the documentation for the [`Gtk.accelerator_parse()` function][Gtk.accelerator_parse()].

Our application already has the `show-hidden-files` action, we'll add the `Ctrl` + `H` keyboard shortcut to it. There's also the default `window.close` action, we'll trigger it with the `Ctrl` + `W` keyboard shortcut.

In `src/Application.js`, in the application startup:

```js {linenostart=13,hl_lines=[4,"9-13"]}
class extends Gtk.Application {
	vfunc_startup() {
		/* ... */
		this.#setupAccelerators();
	}

	/* ... */

	#setupAccelerators() {
		// Setup accelerators
		this.set_accels_for_action('app.show-hidden-files', ['<Control>h']);
		this.set_accels_for_action('window.close', ['<Control>w']);
	}
}
```

Run the application and press the keyboard shortcuts: hidden files will be shown, and the window will close. Easy!

## Adding a Shortcut Controller

Unfortunately, this only works for global actions.

Let's add a `copy` action to our files view in `src/FilesView.js`:

```js {linenostart=76,hl_lines="4-10"}
#setupActions() {
	/* ... */

	const copyAction = new Gio.SimpleAction({
		name: 'copy',
	});
	copyAction.connect('activate', (_action, _params) => {
		console.log('Copy selected files');
	});
	actionGroup.insert(copyAction);

	this.insert_action_group('files-view', actionGroup);
}
```

To trigger this action with the `Ctrl` + `C` keyboard shortcut, we add a [`Gtk.ShortcutController`][Gtk.ShortcutController] as a child of the widget, and add to it a [`Gtk.Shortcut`][Gtk.Shortcut]. The `trigger` property is in the same format as the accelerators, with the angle brackets escaped as `&lt;` and `&gt`. The `action` property can trigger different kinds of [`Gtk.ShortcutAction`][Gtk.ShortcutAction], we'll use the `action(NAME)` syntax to activate an action. The complete syntax can be found in the documentation for the [`parse_string()` function][Gtk.ShortcutAction.parse_string()].

In `data/ui/FilesView.ui`:

```xml {linenostart=3,hl_lines="3-12"}
<template class="FbrFilesView">
	<!-- ... -->
	<child>
		<object class="GtkShortcutController">
			<child>
				<object class="GtkShortcut">
					<property name="trigger">&lt;Control&gt;c</property>
					<property name="action">action(files-view.copy)</property>
				</object>
			</child>
		</object>
	</child>
</template>
```

If you run the application, pressing the keyboard shortcut should trigger the action. Note that it will only work when the files view is active.

## Displaying a Keyboard Shortcuts Window

Adding keyboard shortcuts is nice, but how do our users know they exist?

GTK has a built-in window specifically made to present keyboard shortcuts: [`Gtk.ShortcutsWindow`][Gtk.ShortcutsWindow].

What's more, if we place a UI file containing a `Gtk.ShortcutsWindow` with a `help_overlay` ID in the resources at `gtk/help-overlay.ui`, GTK will automatically load it, add a `win.show-help-overlay` action and present it if the keyboard shortcut `Ctrl` + `?` is pressed.

Let's create a `data/ui/ShortcutsWindow.ui` file. In `data/org.example.filebrowser.data.gresource.xml`, instead of just adding the file, we add an alias to the place GTK expects:

```xml {linenostart=3,hl_lines=3}
<gresource prefix="/org/example/filebrowser">
	<!-- ... -->
	<file alias="gtk/help-overlay.ui">ui/ShortcutsWindow.ui</file>
</gresource>
```

In the `data/ui/ShortcutsWindow.ui` file, we add the expected object:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<object class="GtkShortcutsWindow" id="help_overlay">
	</object>
</interface>
```

You can run the application now and press `Ctrl` + `?`, but the window will be empty. We have to fill it with our keyboard shortcut.

There's a hierarchy of children to comply to: the window accepts [`Gtk.ShortcutsSection`][Gtk.ShortcutsSection] children, which themselves accept [`Gtk.ShortcutsGroup`][Gtk.ShortcutsGroup] children, and inside them we put [`Gtk.ShortcutsShortcut`][Gtk.ShortcutsShortcut] objects describing our keyboard shortcuts. If your application has a lot of shortcuts, they can be neatly organized, there's examples of complex shortcuts windows in [the documentation][Gtk.ShortcutsWindow].

We only need one section, and two groups: one for files shortcuts, and the other for general shortcuts.

Let's start with the section. Since it's the only one, we don't have to give it a name or a title:

```xml {linenostart=3,hl_lines="2-5"}
<object class="GtkShortcutsWindow" id="help_overlay">
	<child>
		<object class="GtkShortcutsSection">
		</object>
	</child>
</object>
```

Then we add the two groups:

```xml {linenostart=5,hl_lines="2-11"}
<object class="GtkShortcutsSection">
	<child>
		<object class="GtkShortcutsGroup">
			<property name="title">Files</property>
		</object>
	</child>
	<child>
		<object class="GtkShortcutsGroup">
			<property name="title">General</property>
		</object>
	</child>
</object>
```

In the "General" group, we add the keyboard shortcuts for closing the window and opening the shortcuts window. Since these are global actions and we defined an accelerator for them, we only have to give to the [`action-name` property][Gtk.ShortcutsShortcut:action-name] the name of the action, and GTK will find the keyboard shortcut itself:

```xml {linenostart=12,hl_lines="3-15"}
<object class="GtkShortcutsGroup">
	<property name="title">General</property>
	<child>
		<object class="GtkShortcutsShortcut">
			<property name="title">Close Window</property>
			<property name="action-name">window.close</property>
		</object>
	</child>
	<child>
		<object class="GtkShortcutsShortcut">
			<property name="title">Keyboard Shortcuts</property>
			<property name="subtitle">Display the keyboard shortcuts window</property>
			<property name="action-name">win.show-help-overlay</property>
		</object>
	</child>
</object>
```

In the "Files" group, the action for showing hidden files is global, but not the one for copying files, so we have to manually set it with the [`accelerator` property][Gtk.ShortcursShortcut:accelerator]:

```xml {linenostart=7,hl_lines="3-14"}
<object class="GtkShortcutsGroup">
	<property name="title">Files</property>
	<child>
		<object class="GtkShortcutsShortcut">
			<property name="title">Show Hidden Files</property>
			<property name="action-name">app.show-hidden-files</property>
		</object>
	</child>
	<child>
		<object class="GtkShortcutsShortcut">
			<property name="title">Copy Selected Files</property>
			<property name="accelerator">&lt;Control&gt;c</property>
		</object>
	</child>
</object>
```

Our window is ready! If you run the application and press `Ctrl` + `?`, the window will display our keyboard shortcuts.

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window.png"
	src-2x="screenshots/window-2x.png"
	src-dark="screenshots/window-dark.png"
	src-dark-2x="screenshots/window-dark-2x.png"
>}}
<!-- spellchecker-enable -->

---

But how can the users know they have to press this keyboard shortcut? In the next chapter, we will add menus, so this window will be easily accessed!


[Gtk.accelerator_parse()]: https://docs.gtk.org/gtk4/func.accelerator_parse.html
[Gtk.Application.set_accels_for_action()]: https://docs.gtk.org/gtk4/method.Application.set_accels_for_action.html
[Gtk.Shortcut]: https://docs.gtk.org/gtk4/class.Shortcut.html
[Gtk.ShortcutAction]: https://docs.gtk.org/gtk4/class.ShortcutAction.html
[Gtk.ShortcutAction.parse_string()]: https://docs.gtk.org/gtk4/ctor.ShortcutAction.parse_string.html
[Gtk.ShortcutController]: https://docs.gtk.org/gtk4/class.ShortcutController.html
[Gtk.ShortcutsGroup]: https://docs.gtk.org/gtk4/class.ShortcutsGroup.html
[Gtk.ShortcutsSection]: https://docs.gtk.org/gtk4/class.ShortcutsSection.html
[Gtk.ShortcutsShortcut]: https://docs.gtk.org/gtk4/class.ShortcutsShortcut.html
[Gtk.ShortcursShortcut:accelerator]: https://docs.gtk.org/gtk4/property.ShortcutsShortcut.accelerator.html
[Gtk.ShortcutsShortcut:action-name]: https://docs.gtk.org/gtk4/property.ShortcutsShortcut.action-name.html
[Gtk.ShortcutsWindow]: https://docs.gtk.org/gtk4/class.ShortcutsWindow.html
