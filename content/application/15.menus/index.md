---
title: Adding Menus
summary: Add a primary menu from a model in a popover
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/menus
weight: 15
slug: menus
---

We'll add a primary menu to our application. Usually, these menus display items that allow the user to perform actions related to the application: opening the preferences, display the keyboard shortcuts, open the about dialog…

So far we have two actions: showing hidden files and opening the keyboard shortcuts window.

## Adding a Menu Button

GTK has a widget dedicated to displaying menus: [`Gtk.MenuButton`][Gtk.MenuButton].

Its [`primary` property][Gtk.MenuButton:primary] tells GTK that the menu is the main menu of the application.

It can take either a custom popover with the [`popover` property][Gtk.MenuButton:popover], or build one automatically from a menu model with the [`menu-model` property][Gtk.MenuButton:menu-model], which is what we'll do.

The [`direction` property][Gtk.MenuButton:direction] sets where the popover will be opened, and changes the icon to an arrow pointing to the according direction. Since we want a menu icon, we'll use the [`Gtk.ArrowType.NONE` value][Gtk.ArrowType].

Let's add the button to our header bar in `src/ui/Window.ui`:

```xml {linenostart=3,hl_lines="5-12"}
<template class="FbrWindow">
	<property name="titlebar">
		<object class="GtkHeaderBar">
			<!-- ... -->
			<child type="end">
				<object class="GtkMenuButton">
					<property name="tooltip-text">Main Menu</property>
					<property name="primary">true</property>
					<property name="direction">none</property>
					<property name="menu-model">mainMenu</property>
				</object>
			</child>
		</object>
	</property>
</template>
```

We used the [`tooltip-text` property][Gtk.Widget:tooltip-text] to display a tooltip when the user hovers the button.

The `menu-model` property points to a menu model with the `mainMenu` ID. This doesn't exist yet, let's create it!

## The Menu Model

GTK allows us to write [`Gio.MenuModel`][Gio.MenuModel] objects in UI templates. The format is described in the [`Gtk.PopoverMenu` documentation][Gtk.PopoverMenu#menu-models].

The root element is `menu`, with a number of `section` elements inside. They contain `item` elements, with `attribute` children elements describing the item with the `name` attribute. The most useful attributes are `label` and `action`, which are the label of the item and the action it will trigger.

If you want a section of horizontal icons, it's possible to add the `attribute` element with the name `display-hint` and the value `horizontal-buttons` to a `section` element, and add the attribute of name `verb-icon` to items inside. You can check the example in the [UI XML cheatsheet][GTK Builder XML].

In our UI template, after the `template` element, we add our menu with our two actions:

```xml {linenostart=2,hl_lines="5-18"}
<interface>
	<template class="FbrWindow">
		<!-- ... -->
	</template>
	<menu id="mainMenu">
		<section>
			<item>
				<attribute name="label">Show Hidden Files</attribute>
				<attribute name="action">app.show-hidden-files</attribute>
			</item>
		</section>
		<section>
			<item>
				<attribute name="label">Keyboard Shortcuts</attribute>
				<attribute name="action">win.show-help-overlay</attribute>
			</item>
		</section>
	</menu>
</interface>
```

If you run the application, it will display our menu, complete with the keyboard shortcuts we defined for each action! Moreover, the `app.show-hidden-action` is a property action that stores its state: when it is active, a check mark is displayed in the menu.

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/popover.png"
	src-2x="screenshots/popover-2x.png"
	src-dark="screenshots/popover-dark.png"
	src-dark-2x="screenshots/popover-dark-2x.png"
>}}
<!-- spellchecker-enable -->

---

Thanks to actions and menu models, we easily added a menu to our application!



<!-- spellchecker-disable -->
[GTK Builder XML]: {{< ref "/cheatsheets/ui-xml/index.md#menu" >}}
<!-- spellchecker-enable -->

[Gio.MenuModel]: https://docs.gtk.org/gio/class.MenuModel.html

[Gtk.ArrowType]: https://docs.gtk.org/gtk4/enum.ArrowType.html
[Gtk.MenuButton]: https://docs.gtk.org/gtk4/class.MenuButton.html
[Gtk.MenuButton:direction]: https://docs.gtk.org/gtk4/property.MenuButton.direction.html
[Gtk.MenuButton:menu-model]: https://docs.gtk.org/gtk4/property.MenuButton.menu-model.html
[Gtk.MenuButton:popover]: https://docs.gtk.org/gtk4/property.MenuButton.popover.html
[Gtk.MenuButton:primary]: https://docs.gtk.org/gtk4/property.MenuButton.primary.html
[Gtk.PopoverMenu#menu-models]: https://docs.gtk.org/gtk4/class.PopoverMenu.html#menu-models
[Gtk.Widget:tooltip-text]: https://docs.gtk.org/gtk4/property.Widget.tooltip-text.html
