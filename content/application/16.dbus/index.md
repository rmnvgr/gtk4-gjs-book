---
title: Interacting With D-Bus
summary: Communicate with the system and other applications with D-Bus
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/dbus
weight: 16
slug: dbus
---

[D-Bus][Freedesktop D-Bus] is a message bus that allows applications to communicate to the system or other applications. This is a powerful mechanism, but it can be a little intimidating at first.

We are going to make our app respect and react to the system appearance: if it's dark, the app will be dark, if it's light, the app will be light.

For that, we'll use [Portals][Flatpak Portals]. They are a set of well-known D-Bus interfaces that permit interaction with the system, even in a sandbox. The [Settings portal][Flatpak Portals#Settings] will allow us to get the user color scheme preference and change the appearance of our application accordingly.

The easiest way to use D-Bus in GJS is to first describe the interface we're interested in, and create a proxy that will connect to a service implementing that interface.

{{% info %}}
If you want to learn everything about D-Bus and GJS, the GJS documentation has [an extensive guide][GJS D-Bus] about it.
{{% /info %}}

## Describing the Interface

An interface is all the methods, properties and signals provided by a service.

To create a proxy to a service implementing the interface, we have to describe it in XML. We don't have to describe all of the methods, properties and signals, only the ones we are interested in.

For the [Settings portal][Flatpak Portals#Settings], we'll use these:

- The [`Read` method][Flatpak Portals#Settings.Read], to read a setting
- The [`SettingChanged` signal,][Flatpak Portals#Settings::SettingChanged] emitted when a setting changes
- The [`version` property][Flatpak Portals#Settings:version], which contains the version of the portal

We'll store the interface in a constant at the top of `src/Application.js`:

```js {linenostart=12}
const SETTINGS_PORTAL_INTERFACE = `

`;
```

Let's start by putting the `interface` element in a `node` element. The `name` attribute is the name of the interface, as given by [the documentation][Flatpak Portals#Settings]:

```xml {linenostart=13}
<node>
	<interface name="org.freedesktop.portal.Settings">
	</interface>
</node>
```

Now, let's describe the [`Read` method][Flatpak Portals#Settings.Read]. We can read in the documentation that it has three arguments: two in the `IN` direction, `namespace` and `key`, both of type `s`, and one in the `OUT` direction, `value`, of type `v`. `IN` means we have to provide these arguments, and `OUT` means we'll get it as a result. The types are described in the [D-Bus specification][D-Bus#type-system].

We add a `method` element with the `name` attribute set to the name of the method, and a child `arg` element for each argument, with its `direction`, `type` and `name` attributes filled:

```xml {linenostart=13,hl_lines="3-7"}
<node>
	<interface name="org.freedesktop.portal.Settings">
		<method name="Read">
			<arg direction="in" type="s" name="namespace"/>
			<arg direction="in" type="s" name="key"/>
			<arg direction="out" type="v" name="value"/>
		</method>
	</interface>
</node>
```

We do the same for the [`SettingChanged` signal][Flatpak Portals#Settings::SettingChanged], with the `signal` element. This time, the `arg` elements don't have a `direction` attribute since we will always get them.

```xml {linenostart=13,hl_lines="4-8"}
<node>
	<interface name="org.freedesktop.portal.Settings">
		 <!-- ... -->
		<signal name="SettingChanged">
			<arg type="s" name="namespace"/>
			<arg type="s" name="key"/>
			<arg type="v" name="value"/>
		</signal>
	</interface>
</node>
```

And finally, for the [`version` property][Flatpak Portals#Settings:version], we use the `property` element. The `access` attribute value tells if it's a read-only property (`read`) or also a writable one (`readwrite`).

```xml {linenostart=13,hl_lines=4}
<node>
	<interface name="org.freedesktop.portal.Settings">
		<!-- ... -->
		<property name="version" access="read" type="u"/>
	</interface>
</node>
```

## Creating the Proxy

Now that we have described the interface, we can use the convenience `Gio.DBusProxy.makeProxyWrapper()` function. It will create for us a [`Gio.DBusProxy` class][Gio.DBusProxy] with convenience methods and properties to the interface's ones.

We'll keep the proxy around in the `#settingsPortalProxy` property. Let's also add a `#connectToSettingsPortal()` method to our `Application` class and call it when it is started:

```js {linenostart=33,hl_lines=[2,6,11]}
class extends Gtk.Application {
	#settingsPortalProxy = null;

	vfunc_startup() {
		/* ... */
		this.#connectToSettingsPortal();
	}

	/* ... */

	#connectToSettingsPortal() {}
});
```

In the method, we create the proxy and instantiate it. We have to give it which bus to use, and under which bus name and object path the interface is: this is where the service can be called.

[The documentation][Flatpak Portals#API] tells us that the portals are on the session bus, under the `org.freedesktop.portal.Desktop` name at the `/org/freedesktop/portal/desktop` path, so we use these values:

```js {linenostart=79,hl_lines="2-8"}
#connectToSettingsPortal() {
	// Create the D-Bus proxy to the settings portal
	const SettingsPortalProxy = Gio.DBusProxy.makeProxyWrapper(SETTINGS_PORTAL_INTERFACE);
	this.#settingsPortalProxy = new SettingsPortalProxy(
		Gio.DBus.session,
		'org.freedesktop.portal.Desktop',
		'/org/freedesktop/portal/desktop'
	);
}
```

## Reading a Property

The Settings portal has a `version` property that we'll use to make sure we'll only interact with a compatible version. To get its value, we simply have to access the `version` property on the proxy:

```js {linenostart=79,hl_lines="4-6"}
#connectToSettingsPortal() {
	/* ... */

	// Check that we're compatible with the settings portal
	if (this.#settingsPortalProxy.version !== 1)
		return;
}
```

If we don't get the expected version, we don't go further.

## Calling a Method

To get the color scheme preference of the user, as told by the documentation, we have to read the `color-scheme` value of the `org.freedesktop.appearance` namespace. Its value is a number: `0` if the user has no preference, `1` if the user prefers a dark appearance, and `2` if they prefer a light appearance.

To use the `Read` method, the proxy wrapper created two convenience methods for us: `ReadSync()` and `ReadRemote()`. The first one is blocking, the second one takes a callback function to deal with the result.

We'll use `ReadSync()`. It will give us an array containing all the `OUT` arguments. The type of the `value` returned argument is `v`, which means it's a [`GLib.Variant`][GLib.Variant], and that we can use the convenience method `recursiveUnpack()` to get its value.

Translated in code, that means:

```js {linenostart=79,hl_lines="4-5"}
#connectToSettingsPortal() {
	/* ... */

	// Get the color scheme and change the appearance accordingly
	const colorScheme = this.#settingsPortalProxy.ReadSync('org.freedesktop.appearance', 'color-scheme')[0].recursiveUnpack();
}
```

Let's add a `#changeAppearance()` method to the application to deal with the color scheme:

```js {linenostart=33,hl_lines="4-17"}
class extends Gtk.Application {
	/* ... */

	#changeAppearance(colorScheme = 0) {
		// Possible color scheme values:
		// 0: no preference
		// 1: prefer dark appearance
		// 2: prefer light appearance
		const gtkSettings = Gtk.Settings.get_default();
		switch (colorScheme) {
			case 1:
				gtkSettings.gtkApplicationPreferDarkTheme = true;
				break;
			default:
				gtkSettings.gtkApplicationPreferDarkTheme = false;
		}
	}
}
```

It changes the [`gtk-application-prefer-dark-theme` property][Gtk.Settings:gtk-application-prefer-dark-theme] of [`Gtk.Settings`][Gtk.Settings] to change the appearance of the application. We call it with the value we get from the portal:

```js {linenostart=79,hl_lines=3}
#connectToSettingsPortal() {
	/* ... */
	this.#changeAppearance(colorScheme);
}
```

Now, when launching the application, it will respect the preferred color scheme!

## Connecting to a Signal

However, if the color scheme changes while the application is running, nothing will happen. That's where signals come in!

We'll connect to the `SettingChanged` signal, and check that the changed setting is the one we're interested in. The proxy has a convenient `connectSignal()` method which takes the signal name and a callback function. It returns an ID that we can pass to the `disconnectSignal()` method.

The callback function takes the proxy, the bus name owner, and an array containing the signal's arguments.

```js {linenostart=79,hl_lines="4-15"}
#connectToSettingsPortal() {
	/* ... */

	// Update the appearance when the color scheme changes
	this.#settingsPortalProxy.connectSignal(
		'SettingChanged',
		(_proxy, _nameOwner, [namespace, key, value]) => {
			// If this is not the setting we want that changed, return
			if (namespace !== 'org.freedesktop.appearance' || key !== 'color-scheme')
				return;
			// Unpack the value
			const colorScheme = value.recursiveUnpack();
			this.#changeAppearance(colorScheme);
		}
	);
}
```

That's it, our application now reacts to color scheme changes!


[D-Bus#type-system]: https://dbus.freedesktop.org/doc/dbus-specification.html#type-system

[Flatpak Portals]: https://flatpak.github.io/xdg-desktop-portal/
[Flatpak Portals#API]: https://flatpak.github.io/xdg-desktop-portal/#idm28
[Flatpak Portals#Settings]: https://flatpak.github.io/xdg-desktop-portal/#gdbus-org.freedesktop.portal.Settings
[Flatpak Portals#Settings.Read]: https://flatpak.github.io/xdg-desktop-portal/#gdbus-method-org-freedesktop-portal-Settings.Read
[Flatpak Portals#Settings:version]: https://flatpak.github.io/xdg-desktop-portal/#gdbus-property-org-freedesktop-portal-Settings.version
[Flatpak Portals#Settings::SettingChanged]: https://flatpak.github.io/xdg-desktop-portal/#gdbus-signal-org-freedesktop-portal-Settings.SettingChanged

[Freedesktop D-Bus]: https://www.freedesktop.org/wiki/Software/dbus/

[GJS D-Bus]: https://gjs.guide/guides/gio/dbus.html

[Gio.DBusProxy]: https://docs.gtk.org/gio/class.DBusProxy.html

[GLib.Variant]: https://docs.gtk.org/glib/struct.Variant.html

[Gtk.Settings]: https://docs.gtk.org/gtk4/class.Settings.html
[Gtk.Settings:gtk-application-prefer-dark-theme]: https://docs.gtk.org/gtk4/property.Settings.gtk-application-prefer-dark-theme.html
