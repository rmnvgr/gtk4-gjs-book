---
title: Translating the App
summary: Translate the application to make it accessible in different languages
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/translation
weight: 17
slug: translation
---

Making our application translatable will make it accessible to a lot more people. Of course, we won't translate it ourselves, but there are tools that will ease contributions from the community.

GTK supports translations through [gettext][GNU gettext]. We will hook it up with our build system, which provides a [gettext helper][Meson i18n.gettext()], to automate the process.

## Translating UI Files

This is the easiest part: everywhere a string needs to be translated, simply add `translatable="yes"` to the tag.

For example, in `data/ui/Window.ui`, to mark the welcome text as translatable:

```xml {linenostart=28}
<object class="FbrWelcomeWidget">
	<property name="welcome-text" translatable="yes">Welcome to our new file browser!</property>
	<!-- ... -->
</object>
```

Add this to all the translatable strings in the UI files, be there plain labels, button labels, tooltip texts, menu items, etc.

If we need to leave a comment for the translators, we simply add an XML comment before:

```xml {linenostart=28,hl_lines=2}
<object class="FbrWelcomeWidget">
	<!-- Keep this cheerful! -->
	<property name="welcome-text" translatable="yes">Welcome to our new file browser!</property>
	<!-- ... -->
</object>
```

The same string can be used in different contexts, and thus may need different translations. To differentiate them, we can add the `context` attribute. Also adding a comment to explain it will be useful to translators.

For example, we use the string "Files" in two different contexts: the view switcher, and the keyboard shortcuts window. To prevent ambiguities, we add a context in `data/ui/ShortcutsWindow.ui`:

```xml {linenostart=7,hl_lines="2-3"}
<object class="GtkShortcutsGroup">
	<!-- Title of a group of keyboard shortcuts -->
	<property name="title" translatable="yes" context="shortcuts">Files</property>
	<!-- ... -->
</object>
```

Now, the translators will have two different "Files" strings to translate.

## Translating JavaScript Strings

Since we separated the presentation from our code, we actually don't have any user-facing strings in our code. But let's say we want to translate the messages we're logging to the console.

We first have to initialize gettext after initializing the package in `src/org.example.filebrowser.js`:

```js {linenostart=13}
// Initialize the translations
imports.package.initGettext();
```

It will set the translation domain to the application ID, and provide the global `_()` function, which marks the string passed to it as translatable.

For example, in `src/FilesView.js`:

```js {linenostart=104}
console.log(_('Selection changed!'));
```

Now, let's say we want to also print the number of files selected. This adds complexity, since we have a variable, and we have to decide if we need to use the singular or plural form of the word "file".

Fortunately, gettext gives us additional functions [to deal with plural forms][GNU gettext Plural forms]. They are accessible in the built-in `gettext` module. We're interested in the `ngettext()` function, which takes the sentence in the singular form, then in the plural form, and the number of elements. gettext will decide which one to use according to the given number.

Let's import it:

```js {linenostart=6}
import { ngettext } from 'gettext';
```

And use it to print different messages depending on the number of selected files:

```js {linenostart=105,hl_lines="3-7"}
onSelectionChanged(selectionmodel, _position, _n_items) {
	const selection = selectionmodel.get_selection();
	console.log(ngettext(
		'One file selected',
		'Several files selected',
		selection.get_size()
	));
	/* ... */
}
```

Of course, that's not really helpful, as it doesn't print the actual number of files. Unfortunately, we can't use template strings, as that won't be recognized. Instead, we'll have to use a placeholder in the string, and replace it after it has been translated:

```js {linenostart=105,hl_lines=["4-5",7]}
onSelectionChanged(selectionmodel, _position, _n_items) {
	const selection = selectionmodel.get_selection();
	console.log(ngettext(
		'%d file selected',
		'%d files selected',
		selection.get_size()
	).replace('%d', selection.get_size()));
	/* ... */
}
```

The console will output the correct sentence according to the number of elements.

## Setting Up the Build System

Now, we have to configure our build system so that it can automatically generate the required files. We create a `po` directory, and import it in `meson.build`, as well as the [`i18n` module][Meson i18n]:

```meson {linenostart=11,hl_lines=3}
# Import the modules
# ...
i18n = import('i18n')
```

```meson {linenostart=15,hl_lines=3}
# Load instructions from subdirectories
# ...
subdir('po')
```

In the `po` directory, we create three files:

- `LINGUAS`, which will contain all the languages the application is translated in
- `meson.build`, containing the instructions for the build system
- `POTFILES`, containing a list of all the files containing translatable strings

In `po/POTFILES`, we add all our UI and source files (even if they don't contain translatable strings, this way we make sure we don't forget one accidentally):

```txt
data/ui/FileListItem.ui
data/ui/FileRow.ui
data/ui/FilesView.ui
data/ui/ShortcutsWindow.ui
data/ui/WelcomeWidget.ui
data/ui/Window.ui

src/Application.js
src/File.js
src/FileRow.js
src/FilesView.js
src/WelcomeWidget.js
src/Window.js
```

In `po/meson.build`, we call the [`gettext()` function][Meson i18n.gettext()]:

```meson
# Set up translations
i18n.gettext(
	APP_ID,
	preset: 'glib'
)
```

It takes as first argument the translation domain of the application. When we initialized gettext in `src/org.example.filebrowser.js`, it automatically set it to the application ID, so we also use it here.

## Generating the Template File

The template file, also named POT file, contains all the translatable strings of our application, and serves as the base for translation files, also names PO files.

When we use the `gettext()` function of Meson, it automatically adds a target to generate these files.

The command to generate the POT file is in the form `meson compile -c {BUILD_DIR} {PROJECT_NAME}-pot`. However, since we're developing in a Flatpak environment, you may not have Meson installed. That's not an issue, we'll use the one from the SDK!

We start by initializing the build directory:

```sh {linenos=none}
flatpak run --command=meson --filesystem=host org.gnome.Sdk builddir
```

Then we generate the POT file:

```sh {linenos=none}
flatpak run --command=meson --filesystem=host org.gnome.Sdk compile -C builddir org.example.filebrowser-pot
```

You can see that a new `po/org.example.filebrowser.pot` file has been created.

## Generating the Translation Files

We start by adding the [language code][GNU gettext Language codes] of the languages we want to translate the application to in the `LINGUAS` file.

For example, if we want to translate the application to French, we put into `LINGUAS`:

```txt
fr
```

The command to use to generate the translation files with Meson is in the form `meson compile -c {BUILD_DIR} {PROJECT_NAME}-update-po`. So, to run it in the Flatpak environment:

```sh {linenos=none}
flatpak run --command=meson --filesystem=host org.gnome.Sdk compile -C builddir org.example.filebrowser-update-po
```

It generate a new `po/fr.po` file, similar to the POT file. However, we can add the translated strings in French in this one, for example for the welcome text:

```gettext {linenostart=68}
#: data/ui/Window.ui:30
msgid "Welcome to our new file browser!"
msgstr "Bienvenue dans votre nouveau navigateur de fichier !"
```

Now, if you run the application by forcing the French locale with `flatpak run --env=LANG=fr_FR.utf-8 org.example.filebrowser`, the text should be displayed in French.

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window.png"
	src-2x="screenshots/window-2x.png"
	src-dark="screenshots/window-dark.png"
	src-dark-2x="screenshots/window-dark-2x.png"
>}}
<!-- spellchecker-enable -->

{{% info %}}
To make it easier for contributors to translate your application, you can use online services like [Weblate](https://weblate.org/fr/).
{{% /info %}}

## Updating Translations

If we make changes to the strings of the application, we can simply run the two previous commands to generate a new POT file and update all the PO files. The previous translations will be kept!


[GNU gettext]: https://www.gnu.org/software/gettext/
[GNU gettext Language codes]: https://www.gnu.org/software/gettext/manual/html_node/Language-Codes.html
[GNU gettext Plural forms]: https://www.gnu.org/software/gettext/manual/html_node/Plural-forms.html

[Meson i18n]: https://mesonbuild.com/i18n-module.html
[Meson i18n.gettext()]: https://mesonbuild.com/i18n-module.html#i18ngettext
