---
title: Packaging the App
summary: Make the application ready for distribution for graphical environments
code: https://gitlab.com/rmnvgr/org.example.filebrowser/-/tree/packaging
weight: 18
slug: packaging
---

Because we used a build system from the beginning, that took care of installing all the files at the correct places, packaging the app is really easy. We only have to add the necessary metadata so that it can be installed and launched from a graphical environment.

## Application Icon

The icon is what makes our application immediately recognizable by our users. It has to convey the function of the app, be visually appealing and yet be simple. To help with that, the GNOME Human Interface Guidelines have a [guide for drawing such an icon](https://developer.gnome.org/hig/guidelines/app-icons.html).

We provide a full color scalable version, in `data/icons/org.example.filebrowser.svg`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<svg width="128" height="128" version="1.1" viewBox="0 0 128 128" xmlns="http://www.w3.org/2000/svg">
	<rect x="24" y="8" width="80" height="112" rx="8" fill="#2b2b2b"/>
	<rect x="24" y="8" width="80" height="104" rx="8" fill="#4f4f4f"/>
	<path d="m64 36 1.8369 19.565 15.134-12.536-12.536 15.134 19.565 1.8369-19.565 1.8369 12.536 15.134-15.134-12.536-1.8369 19.565-1.8369-19.565-15.134 12.536 12.536-15.134-19.565-1.8369 19.565-1.8369-12.536-15.134 15.134 12.536z" fill="#c7c7c7"/>
	<path d="m57.072 56 26.928-30.641-13.072 38.641z" fill="#c5245d"/>
	<path d="m70.928 64-26.928 30.641 13.072-38.641z" fill="#fff"/>
</svg>
```

<!-- spellchecker-disable -->
{{< svg-use width="128" height="128" id="icon-color" >}}
<!-- spellchecker-enable -->

As well as a symbolic version that the desktop environment may use if it needs a monochrome or high contrast icon, in `data/icons/org.example.filebrowser-symbolic.svg`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<svg width="16" height="16" version="1.1" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
	<circle cx="8" cy="8" r="6" opacity=".5"/>
	<path d="m6.2679 7.0002 5.7321-5.9282-2.2679 7.9282-5.7321 5.9282z"/>
</svg>
```

<!-- spellchecker-disable -->
{{< svg-use width="16" height="16" id="icon-symbolic" >}}
<!-- spellchecker-enable -->

Because these icons need to be accessible from outside the application, we can't ship them in the resources. Instead, we have to install them on the system, in a well-known place: the `apps` category of the `hicolor` icon theme, following the [Icon Theme specification][XDG Icon Theme].

In `data/meson.build`:

```meson {linenostart=16}
# Install the application icons
install_data(
	'icons' / APP_ID + '.svg',
	install_dir: get_option('datadir') / 'icons' / 'hicolor' / 'scalable' / 'apps'
)
install_data(
	'icons' / APP_ID + '-symbolic.svg',
	install_dir: get_option('datadir') / 'icons' / 'hicolor' / 'symbolic' / 'apps'
)
```

And in `meson.build`, we tell the build system to update the icon cache after installation:

```meson {linenostart=21,hl_lines=3}
gnome.post_install(
	# ...
	gtk_update_icon_cache: true
)
```

## Desktop Launcher

Until now, we have run our application with a command line. That is not ideal for regular users, it would be better if they could graphically launch the application with a shortcut. Fortunately, that's what the [Desktop Entry Specification][Freedesktop Desktop Entry Specification] is about.

We have to write two files: one for the D-Bus service that will allow our application to be activated, and a second for presenting a shortcut to the user in their desktop environment.

Let's start with the service file. In `data/org.example.filebrowser.service`, we put:

```ini
[D-BUS Service]
Name=@APP_ID@
Exec=@BINDIR@/@APP_ID@ --gapplication-service
```

The `Name` field is the bus name that the application will own, the `Exec` field is the command that will be run. It will start the application in a special service mode, waiting to be activated.

Now, for the desktop file. In `data/org.example.filebrowser.desktop`, we put:

```ini
[Desktop Entry]
Name=File Browser
Comment=Browse files on your system
Icon=@APP_ID@
Exec=@BINDIR@/@APP_ID@
Type=Application
DBusActivatable=true
Categories=System;FileManager;
# Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
Keywords=File;Browser;
```

The `Name`, `Comment` and `Keywords` fields are translatable, so don't forget to add this file to `po/POTFILES`.

Now, in `data/meson.build`, we have to tell the build system where to install these files, as well as replace the placeholder values and translate the desktop file:

```meson {linenostart=26}
# Translate and install the desktop file
i18n.merge_file(
	type: 'desktop',
	input: configure_file(
		input: APP_ID + '.desktop',
		output: APP_ID + '.desktop.in',
		configuration: {
			'APP_ID': APP_ID,
			'BINDIR': get_option('prefix') / get_option('bindir')
		}
	),
	output: APP_ID + '.desktop',
	po_dir: meson.project_source_root() / 'po',
	install: true,
	install_dir: get_option('datadir') / 'applications'
)

# Install the D-Bus service file
configure_file(
	input: APP_ID + '.service',
	output: APP_ID + '.service',
	configuration: {
		'APP_ID': APP_ID,
		'BINDIR': get_option('prefix') / get_option('bindir')
	},
	install: true,
	install_dir: get_option('datadir') / 'dbus-1' / 'services'
)
```

And in `meson.build`, we tell the build system to update the desktop database after installation:

```meson {linenostart=21,hl_lines=3}
gnome.post_install(
	# ...
	update_desktop_database: true
)
```

## AppStream Metadata

Our future users need a way of finding the application in their application store. For that, we have to provide several metadata, following the [AppStream specification][Freedesktop AppStream].

In `data/org.example.filebrowser.metainfo.xml`, we put this minimal metadata:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
	<id>@APP_ID@</id>

	<name>File Browser</name>
	<summary>Browse files on your system</summary>
	<description>
		<p>A wonderful application designed to use a lot of GTK4 features.</p>
	</description>

	<metadata_license>CC-BY-SA-4.0</metadata_license>
	<project_license>GPL-3.0-or-later</project_license>

	<launchable type="desktop-id">@APP_ID@.desktop</launchable>
	<provides>
		<binary>@APP_ID@</binary>
		<dbus type="user">@APP_ID@</dbus>
	</provides>
	<translation type="gettext">@APP_ID@</translation>
</component>
```

It tells:

- The `id` of the application
- Its `name`
- A short `summary`
- A longer `description`
- The [SPDX License IDs][SPDX License IDs] of the metadata and the application
- The name of the desktop file
- The `binary` and `dbus` name the application `provides`
- The `translation` domain, so that the user knows if the application is translated in their language

A lot more information can be added, like screenshots, release notes, links to the website, the repository, donations, etc. The [AppStream documentation][Freedesktop AppStream] describes all the possible elements, with a lot of examples. The more metadata we add, the more attractive the application will appear, and the more likely to install it the user will be!

{{% info %}}
You can easily generate the metadata with the [AppStream Metainfo Creator][Freedesktop AppStream Metainfo Creator].
{{% /info %}}

The metadata can be translated, so add this file to `po/POTFILES`.

Now, in `data/meson.build`, we tell the build system to translate and install it in the expected place:

```meson {linenostart=55}
# Translate and install the AppStream metadata
i18n.merge_file(
	input: configure_file(
		input: APP_ID + '.metainfo.xml',
		output: APP_ID + '.metainfo.xml.in',
		configuration: {
			'APP_ID': APP_ID,
		}
	),
	output: APP_ID + '.metainfo.xml',
	type: 'xml',
	po_dir: meson.project_source_root() / 'po',
	install: true,
	install_dir: get_option('datadir') / 'metainfo'
)
```

---

Our application is now ready to be distributed! Since we used Flatpak all along, [Flathub][Flathub] would be a natural place to publish it.


<!-- spellchecker-disable -->
{{< svg-symbol viewbox="0 0 128 128" id="icon-color" >}}
<rect x="24" y="8" width="80" height="112" rx="8" fill="#2b2b2b"/>
<rect x="24" y="8" width="80" height="104" rx="8" fill="#4f4f4f"/>
<path d="m64 36 1.8369 19.565 15.134-12.536-12.536 15.134 19.565 1.8369-19.565 1.8369 12.536 15.134-15.134-12.536-1.8369 19.565-1.8369-19.565-15.134 12.536 12.536-15.134-19.565-1.8369 19.565-1.8369-12.536-15.134 15.134 12.536z" fill="#c7c7c7"/>
<path d="m57.072 56 26.928-30.641-13.072 38.641z" fill="#c5245d"/>
<path d="m70.928 64-26.928 30.641 13.072-38.641z" fill="#fff"/>
{{< /svg-symbol >}}

{{< svg-symbol viewbox="0 0 16 16" id="icon-symbolic" >}}
<circle cx="8" cy="8" r="6" opacity=".5" fill="currentColor"/>
<path d="m6.2679 7.0002 5.7321-5.9282-2.2679 7.9282-5.7321 5.9282z" fill="currentColor"/>
{{< /svg-symbol >}}
<!-- spellchecker-enable -->


[Flathub]: https://flathub.org/

[Freedesktop AppStream]: https://www.freedesktop.org/software/appstream/docs/
[Freedesktop AppStream Metainfo Creator]: https://www.freedesktop.org/software/appstream/metainfocreator/
[Freedesktop Desktop Entry Specification]: https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html

[SPDX License IDs]: https://spdx.dev/ids/

[XDG Icon Theme]: https://specifications.freedesktop.org/icon-theme-spec/icon-theme-spec-latest.html
