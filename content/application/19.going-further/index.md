---
title: Going Further
summary: Some additional things to improve the application
code:
weight: 19
slug: going-further
---

We have finished our application, and that's the end of this book!

Fortunately, there's still a lot to discover. Here are some additional things you may want to look into.

## Adding a Help System

No matter how simple or complex you application is, your users may want to learn more about using it.

[Yelp][Yelp] is a help viewer for the GNOME desktop, you can write help pages for it in the [Mallard][Mallard] format.

The GNOME module of the Meson build system has a [`yelp()` function][Meson gnome.yelp()] that will automatically install the help pages:

```meson
gnome.yelp(
	APP_ID,
	sources: [
		'index.page'
	],
)
```

The help system can be opened from within an application with the [`Gtk.show_uri()` function][Gtk.show_uri()]:

```js
Gtk.show_uri(parent_window, `help:${application_id}`, Gdk.CURRENT_TIME);
```

## Using Libadwaita

If you specifically target the GNOME platform, [Libadwaita][Libadwaita] is a library that will make your life easier.

It gives access to widgets implementing the GNOME interface guidelines, adaptive widgets, automatically switching the appearance according to the system color scheme, and much more.

To start using Libadwaita, you can simply replace `Gtk.Application` with [`Adw.Application`][Adw.Application]:

```js
import Adw from 'gi://Adw';

export const Application = GObject.registerClass({
	GTypeName: 'FbrApplication'
}, class extends Adw.Application {}
```

This is what our application would look like if we used Libadwaita widgets:

<!-- spellchecker-disable -->
{{< picture
	src="screenshots/window.png"
	src-2x="screenshots/window-2x.png"
	src-dark="screenshots/window-dark.png"
	src-dark-2x="screenshots/window-dark-2x.png"
>}}
<!-- spellchecker-enable -->


[Libadwaita]: https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/

[Mallard]: http://projectmallard.org/

[Meson gnome.yelp()]: https://mesonbuild.com/Gnome-module.html#gnomeyelp

[Yelp]: http://yelp.io/

[Adw.Application]: https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.Application.html

[Gtk.show_uri()]: https://docs.gtk.org/gtk4/func.show_uri.html
