---
title: Building an Application
summary:
weight: 2

---

In this section, we'll build a complete application, making use of as many GTK features as possible.

It will let us display the files in a directory, this is what it will look like when we are finished:

<!-- spellchecker-disable -->
{{< picture
	src="window.png"
	src-2x="window-2x.png"
	src-dark="window-dark.png"
	src-dark-2x="window-dark-2x.png"
>}}
<!-- spellchecker-enable -->

At each step, we'll discover a new GTK feature. Let's start!
