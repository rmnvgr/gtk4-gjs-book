---
title: Custom GObject
summary: Practical example of a full GObject implementation in GJS
slug: gobject
---

## Minimal GObject Implementation

```js
import GObject from 'gi://GObject';

const MyObject = GObject.registerClass(class extends GObject.Object {});

const myObject = new MyObject();
```

## Advanced GObject Implementation

### Constructor

```js
import GObject from 'gi://GObject';

const MyObject = GObject.registerClass(class extends GObject.Object {
	constructor(params = {}) {
		super(params);
	}
});
```

### Naming the GObject

```js
import GObject from 'gi://GObject';

const MyObject = GObject.registerClass({
	GTypeName: 'MyObject',
}, class extends GObject.Object {});
```

### Properties

#### Simple Read-Write Property

```js
import GObject from 'gi://GObject';

const MyObject = GObject.registerClass({
	Properties: {
		'property': GObject.ParamSpec.string(
			'property', // Name
			'Property', // Nick
			'A property that can be read and written', // Blurb
			GObject.ParamFlags.READWRITE, // Flags
			null // Default value
		),
	},
}, class extends GObject.Object {});

const myObject = new MyObject({ property: 'Hello World' });

console.log(myObject.property); // Hello World
```

#### Read-Only Property

```js
import GObject from 'gi://GObject';

const MyObject = GObject.registerClass({
	Properties: {
		'read-only-property': GObject.ParamSpec.string(
			'read-only-property', // Name
			'Read-only property', // Nick
			'A property that can only be read', // Blurb
			GObject.ParamFlags.READABLE, // Flags
			null // Default value
		),
	},
}, class extends GObject.Object {
	get readOnlyProperty() {
		return 'Default value';
	}
});

const myObject = new MyObject();

console.log(myObject.readOnlyProperty); // Default value
```

#### Custom Getter and Setter

```js
import GObject from 'gi://GObject';

const MyObject = GObject.registerClass({
	Properties: {
		'custom-property': GObject.ParamSpec.string(
			'custom-property', // Name
			'custom property', // Nick
			'A property with custom getter and setter', // Blurb
			GObject.ParamFlags.READWRITE, // Flags
			null // Default value
		),
	},
}, class extends GObject.Object {
	get customProperty() {
		// Return null if the value is undefined, or the value
		return this._customProperty === undefined ? null : this._customProperty;
	}

	set customProperty(value) {
		// If the value is the same, return
		if (value === this._customProperty)
			return;
		// Transform the value
		this._customProperty = value.toUpperCase();
		// Notify other GObjects that the property changed
		this.notify('custom-property');
	}
});

const myObject = new MyObject({ customProperty: 'Hello World' });

console.log(myObject.customProperty); // HELLO WORLD
```

### Signals

#### Simple Signal

```js
import GObject from 'gi://GObject';

const MyObject = GObject.registerClass({
	Signals: {
		'ready': {},
	},
}, class extends GObject.Object {
	constructor(params = {}) {
		super(params);
		this.emit('ready');
	}
});
```

### Implementing Interfaces

```js
import Gio from 'gi://Gio';
import GObject from 'gi://GObject';

const MyListModel = GObject.registerClass({
	Implements: [Gio.ListModel],
}, class extends GObject.Object {
	// Store items
	#items = [];

	// Implement virtual methods of the interface
	vfunc_get_item_type() {
		return GObject.Object;
	}

	vfunc_get_item(position) {
		return this.#items[position] || null;
	}

	vfunc_get_n_items() {
		return this.#items.length;
	}

	// Custom method
	add(item) {
		// Check the item is of the correct type
		if (!(item instanceof GObject.Object))
			throw new TypeError('Only GObjects can be added');
		this.#items.push(item);
	}
});

const listModel = new MyListModel();
listModel.add(new GObject.Object());

console.log(listModel.get_n_items()); // 1
```
