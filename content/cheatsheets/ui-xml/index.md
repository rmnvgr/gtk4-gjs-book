---
title: GTK Builder XML
summary: Practical examples of GTK4 Builder XML for writing user interfaces
slug: ui-xml
---

## Composite Widget

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<template class="MyWidget">
		<property name="layout-manager">
			<object class="GtkBinLayout"/>
		</property>
		<child>
			<object class="GtkLabel">
				<property name="label">Hello World!</property>
			</object>
		</child>
	</template>
</interface>
```

## Binding Properties

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<object class="GtkBox">
		<child>
			<object class="GtkCheckButton" id="checkButton">
				<property name="label">Sensitive button</property>
			</object>
		</child>
		<child>
			<object class="GtkButton">
				<property name="label">Button</property>
				<property name="sensitive" bind-source="checkButton" bind-property="active" bind-flags="sync-create"/>
			</object>
		</child>
	</object>
</interface>
```

## Signal Handler

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<object class="GtkButton">
		<property name="label">Click Me</property>
		<signal name="clicked" handler="onButtonClicked"/>
	</object>
</interface>
```

## Menu

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<object class="GtkMenuButton">
		<property name="menu-model">menu</property>
	</object>
	<menu id="menu">
		<!-- Horizontal icons -->
		<section>
			<attribute name="display-hint">horizontal-buttons</attribute>
			<item>
				<attribute name="label">Copy</attribute>
				<attribute name="action">app.copy</attribute>
				<attribute name="verb-icon">edit-copy-symbolic</attribute>
			</item>
			<item>
				<attribute name="label">Paste</attribute>
				<attribute name="action">app.paste</attribute>
				<attribute name="verb-icon">edit-paste-symbolic</attribute>
			</item>
		</section>
		<!-- Regular buttons -->
		<section>
			<item>
				<attribute name="label">Close</attribute>
				<attribute name="action">win.close</attribute>
			</item>
		</section>
	</menu>
</interface>
```

## Translatable Strings

```xml
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<object class="GtkLabel">
		<!-- Message to translators -->
		<property name="label" translatable="yes" context="welcome-screen">Hello World!</property>
	</object>
</interface>
```
