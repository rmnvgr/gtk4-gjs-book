---
title: Custom Widget
summary: Practical example of a full GTK4 widget implementation in GJS
slug: widget
---

## Loading a Template

### From File

```js {hl_lines=6}
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk?version=4.0';

const MyWidget = GObject.registerClass({
	GTypeName: 'MyWidget',
	Template: 'file:///path/to/the/template.ui',
}, class extends Gtk.Widget {});
```

### From Resources

```js {hl_lines=6}
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk?version=4.0';

const MyWidget = GObject.registerClass({
	GTypeName: 'MyWidget',
	Template: 'resource:///path/to/the/template.ui',
}, class extends Gtk.Widget {});
```

### From String

```js {hl_lines=22}
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk?version=4.0';

const template = `
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<template class="MyWidget">
		<property name="layout-manager">
			<object class="GtkBinLayout"/>
		</property>
		<child>
			<object class="GtkLabel">
				<property name="label">Hello World</property>
			</object>
		</child>
	</template>
</interface>
`;

const MyWidget = GObject.registerClass({
	GTypeName: 'MyWidget',
	Template: new TextEncoder().encode(template),
}, class extends Gtk.Widget {});
```

## Children

### Public Children

```js {hl_lines=[12,23,27]}
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk?version=4.0';

const template = `
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<template class="MyWidget">
		<property name="layout-manager">
			<object class="GtkBinLayout"/>
		</property>
		<child>
			<object class="GtkLabel" id="label">
				<property name="label">Hello World</property>
			</object>
		</child>
	</template>
</interface>
`;

const MyWidget = GObject.registerClass({
	GTypeName: 'MyWidget',
	Template: new TextEncoder().encode(template),
	Children: ['label'],
}, class extends Gtk.Widget {
	constructor(params = {}) {
		super(params);
		console.log(this.label.label); // Hello World
	}
});
```

### Internal Children

```js {hl_lines=[12,23,27]}
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk?version=4.0';

const template = `
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<template class="MyWidget">
		<property name="layout-manager">
			<object class="GtkBinLayout"/>
		</property>
		<child>
			<object class="GtkLabel" id="label">
				<property name="label">Hello World</property>
			</object>
		</child>
	</template>
</interface>
`;

const MyWidget = GObject.registerClass({
	GTypeName: 'MyWidget',
	Template: new TextEncoder().encode(template),
	InternalChildren: ['label'],
}, class extends Gtk.Widget {
	constructor(params = {}) {
		super(params);
		console.log(this._label.label); // Hello World
	}
});
```

## Signal Handler

```js {hl_lines=[14,"25-27"]}
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk?version=4.0';

const template = `
<?xml version="1.0" encoding="UTF-8"?>
<interface>
	<template class="MyWidget">
		<property name="layout-manager">
			<object class="GtkBinLayout"/>
		</property>
		<child>
			<object class="GtkButton">
				<property name="label">Click Me</property>
				<signal name="clicked" handler="onButtonClicked"/>
			</object>
		</child>
	</template>
</interface>
`;

const MyWidget = GObject.registerClass({
	GTypeName: 'MyWidget',
	Template: new TextEncoder().encode(template),
}, class extends Gtk.Widget {
	onButtonClicked(button) {
		console.log(button.label); // Click Me
	}
});
```
