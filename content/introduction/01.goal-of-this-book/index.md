---
title: Goal of This Book
summary: What the book will and won't teach you about GTK4 and GJS
slug: goal-of-this-book
weight: 1
---

This book will guide you through the creation of a GTK4+GJS application, using as many useful GTK4 features along the way.

It wants to get you started quickly through practical examples. Each chapter depends on the code written in previous one, but you can still read them out of order if you simply need examples on a specific topic.

It is not meant to be a thorough documentation of all GTK4 and GJS features, which is the role of their [respective documentations][Useful Resources]. It will however give you enough knowledge so that you feel confident diving into these documentations and begin experimenting on your own.

I hope you'll enjoy your journey, now let's create an app!


[Useful Resources]: {{< ref "/introduction/04.resources/index.md" >}}
