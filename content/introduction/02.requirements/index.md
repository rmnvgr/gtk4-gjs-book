---
title: Requirements
summary: What is needed to follow the book
weight: 2
slug: requirements
---

In order to follow this book, a few things are required.

## Knowledge of the JavaScript Language

The books assumes you already have a basic understanding of the JavaScript language: creation of classes, inheritance, array manipulation, modules, etc. It builds upon it to explain how GJS works.

If you need a refresher, the [MDN documentation][MDN JavaScript] has a lot of tutorials and the full JavaScript API.

## Flatpak Environment

We'll use [Flatpak][Flatpak] in order to have a consistent development environment, isolated from your system. Of course, this is only a recommendation, ultimately you're free to do whatever you want, but we strongly believe Flatpak has powerful advantages when it comes to desktop application development, let alone application distribution.

To learn more about Flatpak and make sure it's installed on your system, head over to the [Flatpak official website][Flatpak].

## The Flathub Repository

[Flathub][Flathub] is the most popular Flatpak repository. We'll use it to download the required SDK and runtime for our application. Later, you can publish your applications on Flathub to make them available to a wide user base.

Follow the instructions on the [Flathub website][Flathub] to add the repository to your system.


[Flathub]: https://flathub.org/home

[Flatpak]: https://flatpak.org/

[MDN JavaScript]: https://developer.mozilla.org/en-US/docs/Web/JavaScript
