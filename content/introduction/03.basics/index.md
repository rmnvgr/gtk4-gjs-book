---
title: The Basics
summary: Description of GJS, GTK, and associated libraries and concepts
weight: 3
slug: basics
---

## What Is GTK?

[GTK][GTK] is an application toolkit. It provides widgets and facilities to build powerful graphical applications. It is written in C, but provides language bindings to a lot of other programming languages. This is why we can use GTK with GJS.

It relies on several other libraries that we'll also have to use:

- [GLib][GLib]: Core library with data types, main loop, utility functions
- [Gio][Gio]: file system access, D-Bus communication, application settings
- [GObject][GObject]: Object system, read more [below](#what-is-gobject)

## What Is GJS?

GJS is a JavaScript engine, like a browser or Node.js. It is centered around using GObject introspected libraries, that is, system libraries that are made available to other programming languages through language bindings, like GTK.

To access these libraries, you have to import them from a special place, `gi://`. The [GJS API documentation][GJS API] lists some libraries that are introspected.

For example, to import GTK:

```js
import Gtk from 'gi://Gtk';
```

However, if there's multiple versions of a library installed on the system (for example, GTK3 and GTK4), you have to specify which version you want:

```js
import Gtk from 'gi://Gtk?version=4.0';
```

{{% info %}}
How about Node.js modules? If they are pure JavaScript modules, it's possible to use them. However, if they make use of Node.js-specific APIs, they will not work.
{{% /info %}}

## What Is GObject?

GObject is an object-oriented library for C. Since JavaScript is also an object-oriented language, GJS has facilities in place to allow us to use GObjects as JavaScript objects. For example, GTK widgets are GObjects, and we can instantiate them, access their properties and call their methods like regular JavaScript classes.

To make our own JavaScript classes available as GObject, we can use this utility function:

```js
import GObject from 'gi://GObject';

const MyClass = GObject.registerClass(class extends GObject.Object {
	/* Regular JavaScript class implementation */
});
```

It's possible to pass a configuration object as the first parameter of the `registerClass()` function to tweak the behavior of our GObject, for example for defining properties or signals. To learn more about it, head over to the [GObject subclassing section of the GJS Guide][GJS GObject Subclassing], or to the [GObject cheatsheet][GObject Cheatsheet] for practical examples.

## What Is Meson?

[Meson][Meson] is a build system. It will take care for us of installing our application files to the correct places. Furthermore, it ships with modules that makes it even more easier to perform some needed tasks.

Using a well-known build system ensures that people wanting to build and distribute our application won't run into trouble.

## What Is Flatpak?

[Flatpak][Flatpak] gives us a well-known environment for developing and distributing desktop applications. It is based around runtimes: if an application builds and runs with a specific runtime on your computer, it will also build and run on someone else's computer if they use Flatpak with the same runtime.

In this book, we'll use the GNOME runtime, as it provides the GJS, GTK4 and Meson dependencies we need for our application. It is split in two: the platform, that only contains libraries needed to run applications, and the SDK, that contains libraries and tools needed to build applications.

Our use of Flatpak will be very basic, please refer to the [official Flatpak documentation][Flatpak] to learn more about its capabilities.


<!-- spellchecker-disable -->
[GObject Cheatsheet]: {{< ref "/cheatsheets/gobject/index.md" >}}
<!-- spellchecker-enable -->

[Flatpak]: https://flatpak.org/

[Gio]: https://docs.gtk.org/gio/

[GJS API]: https://gjs-docs.gnome.org/

[GJS GObject Subclassing]: https://gjs.guide/guides/gobject/subclassing.html

[GLib]: https://docs.gtk.org/glib/

[GObject]: https://docs.gtk.org/gobject/

[GTK]: https://gtk.org/

[Meson]: https://mesonbuild.com/
