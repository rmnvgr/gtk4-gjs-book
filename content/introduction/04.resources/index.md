---
title: Helpful Resources
summary: Links to helpful websites related to GTK4 and GJS development
weight: 4
slug: resources
---

- https://gjs.guide/: GJS Guide, the official website of GJS, with a lot of documentation and examples
- https://gjs-docs.gnome.org/: GJS API reference
- https://docs.gtk.org/: GTK and associated libraries documentation
- https://developer.gnome.org/: GNOME platform documentation, including general documentation, tutorials and guidelines
- https://discourse.gnome.org/: GNOME discussion forums, if you have development questions
