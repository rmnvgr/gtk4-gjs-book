{{- $re := `^(?:(\d+)\.)?(.*)` -}}
{{- $weight := .Name | replaceRE $re "$1" -}}
{{- $name := .Name | replaceRE $re "$2" -}}
---
title: {{ humanize $name | title }}
summary:
code:
weight: {{ with $weight }}{{ . }}{{ else }}0{{ end }}
slug: {{ $name }}
---
