// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

export function initCopyCode() {
	Array.from(document.getElementsByClassName('highlight')).forEach(codeBlock => {
		codeBlock.addEventListener('copy', e => {
			e.preventDefault();
			const selection = window.getSelection();
			e.clipboardData.setData('text/plain', selection.toString());
		});

		const button = document.createElement('button');
		button.classList.add('copy-code');
		button.textContent = 'Copy Code';
		button.title = 'Copy Code';

		let previousTimeout;
		button.addEventListener('click', () => {
			const selection = window.getSelection();
			selection.removeAllRanges();
			selection.selectAllChildren(codeBlock);
			navigator.clipboard.writeText(selection.toString());
			button.blur();
			button.classList.add('copied');
			if (previousTimeout) {
				clearTimeout(previousTimeout);
			}
			previousTimeout = setTimeout(() => {
				button.classList.remove('copied');
				previousTimeout = null;
			}, 2000);
		});

		codeBlock.append(button);
	});
}
