// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

'use strict';

import { initSearch } from './search';
import { initSidebar } from './sidebar';
import { initScrollTracker } from './scrolltracker';
import { initCopyCode } from './copycode';

document.documentElement.classList.add('script');

initSearch();
initSidebar();
initScrollTracker();
initCopyCode();

document.body.offsetHeight; // Force redraw

document.documentElement.classList.add('ready');
