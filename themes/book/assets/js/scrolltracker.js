// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

let currentHeader;

const headers = [];
const links = [];

export function initScrollTracker() {
	const toc = document.getElementById('content-toc');
	const content = document.getElementById('content-body');

	if (!toc || !content)
		return;

	links.push(...Array.from(toc.getElementsByTagName('a')));

	headers.push(...Array.from(content.getElementsByTagName('h2')));
	headers.push(...Array.from(content.getElementsByTagName('h3')));
	headers.sort((a, b) => a.offsetTop > b.offsetTop);

	let scroll = 0;
	let ticking = false;

	document.addEventListener('scroll', () => {
		scroll = window.scrollY;
		// Only run on a new frame
		if (!ticking) {
			window.requestAnimationFrame(() => {
				updateCurrentLink(scroll);
				ticking = false;
			});
		}
		ticking = true;
	});
}

function updateCurrentLink(scroll) {
	const threshold = window.innerHeight * 0.2;

	let newCurrentHeader;
	for (const header of headers) {
		if (header.offsetTop < scroll + threshold)
			newCurrentHeader = header;
		else
			break;
	}

	if (newCurrentHeader !== currentHeader)
		currentHeader = newCurrentHeader;
	else
		return;

	links.forEach(link => {
		link.parentElement.classList.remove('current');
		if (!currentHeader)
			return;
		if (link.hash.slice(1) === currentHeader.id)
			link.parentElement.classList.add('current');
	});
}
