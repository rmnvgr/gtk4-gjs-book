// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import * as params from '@params';


class Word {
	constructor({ term, weight }) {
		this.term = term;
		this.weight = weight;
	}
}


class Page {
	constructor({ title, content, permalink, section, summary }) {
		this.title = title;
		this.content = content;
		this.permalink = permalink;
		this.section = section;
		this.summary = summary;
	}
}


class Result {
	constructor({ page, weight }) {
		this.page = page;
		this.weight = weight;
	}
}


const tags = new Map();

export function initSearch() {
	const pages = JSON.parse(params.pages);
	pages.map(page => new Page(page)).forEach(page => {
		const words = [];
		words.push(...tagify(page.title).map(term => new Word({ term, weight: 50 })));
		words.push(...tagify(page.summary).map(term => new Word({ term, weight: 10 })));
		words.push(...tagify(page.section).map(term => new Word({ term, weight: 1 })));
		words.push(...tagify(page.content).map(term => new Word({ term, weight: 1 })));
		words.filter(word => word.term.length > 1).forEach(word => {
			if (!tags.has(word.term)) {
				tags.set(word.term, [ new Result({ page, weight: word.weight }) ]);
			} else {
				const results = tags.get(word.term);
				const result = results.find(result => result.page === page);
				if (result)
					result.weight += word.weight;
				else
					results.push(new Result({ page, weight: word.weight }));
			}
		});
	});

	const searchInput = document.getElementById('search-input');
	const clearSearchButton = document.getElementById('search-clear-button');

	const clearSearchInput = () => {
		searchInput.value = '';
		updateClearSearchButton();
		search();
	};

	const updateClearSearchButton = () => {
		clearSearchButton.classList.toggle('visible', !!searchInput.value);
		clearSearchButton.tabIndex = !searchInput.value ? -1 : 0;
	};

	searchInput.addEventListener('input', () => {
		updateClearSearchButton();
		search(searchInput.value);
	});

	searchInput.addEventListener('keyup', e => {
		if (e.key === 'Escape') {
			clearSearchInput();
			searchInput.blur();
			e.preventDefault();
		}
	});

	clearSearchButton.addEventListener('click', () => {
		clearSearchInput();
		searchInput.focus();
	});

	document.addEventListener('keyup', e => {
		if (e.key === 's' && document.activeElement === document.body) {
			searchInput.focus();
			e.preventDefault();
		}
	});

	const query = new URL(window.location).searchParams.get('q');
	if (query)
		searchInput.value = query;

	updateClearSearchButton();
	search(searchInput.value);
}

function search(text = '') {
	const oldSiteResults = document.getElementById('search-results');

	if (oldSiteResults)
		oldSiteResults.remove();

	const url = new URL(window.location);
	text ? url.searchParams.set('q', text) : url.searchParams.delete('q');
	window.history.replaceState(null, '', url);

	if (!text)
		return;

	const results = [];

	const queries = tagify(text);
	queries.forEach(query => {
		const matches = Array.from(tags.keys()).filter(tag => tag.includes(query));
		matches.forEach(tag => {
			const tagResults = tags.get(tag);
			tagResults.forEach(tagResult => {
				const result = results.find(result => result.page === tagResult.page);
				if (result)
					result.weight += tagResult.weight;
				else
					results.push(new Result(tagResult));
			});
		});
	});

	results.sort((a, b) => a.weight < b.weight);

	const siteSearch = document.getElementById('site-search');

	const searchResults = document.createElement('aside');
	searchResults.id = 'search-results';
	searchResults.classList.add('search-results');

	const searchResultsTitle = document.createElement('h2');
	searchResultsTitle.textContent = 'Search Results For ';
	searchResults.append(searchResultsTitle);

	const searchResultsTitleTerm = document.createElement('q');
	searchResultsTitleTerm.textContent = text;
	searchResultsTitle.append(searchResultsTitleTerm);

	if (results.length === 0) {
		const noResultsText = document.createElement('p');
		noResultsText.textContent = 'No results.';
		searchResults.append(noResultsText);
	} else {
		const resultsList = document.createElement('ul');
		searchResults.append(resultsList);

		results.slice(0, 10).forEach(result => {
			const resultItem = document.createElement('li');
			resultItem.classList.add('result');
			resultsList.append(resultItem);

			const resultLink = document.createElement('a');
			resultLink.href = result.page.permalink;
			resultItem.append(resultLink);

			const resultTitle = document.createElement('span');
			resultTitle.classList.add('result-title');
			resultTitle.textContent = result.page.title;
			resultLink.append(resultTitle);

			if (result.page.title !== result.page.section) {
				const resultSection = document.createElement('span');
				resultSection.classList.add('result-section');
				resultSection.textContent = result.page.section;
				resultLink.append(resultSection);
			}

			const resultSummary = document.createElement('span');
			resultSummary.classList.add('result-summary');
			resultSummary.textContent = result.page.summary;
			resultLink.append(resultSummary);
		});
	}

	siteSearch.append(searchResults);
}

function tagify(text) {
	return text.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f-]/g, '').replace(/\W|\d/g, ' ').trim().split(/\s/);
}
