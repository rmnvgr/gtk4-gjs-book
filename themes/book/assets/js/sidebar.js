// SPDX-FileCopyrightText: 2022 Romain Vigier <contact AT romainvigier.fr>
//
// SPDX-License-Identifier: GPL-3.0-or-later

export function initSidebar() {
	const sidebar = document.getElementById('sidebar');

	const openSidebarButton = document.getElementById('sidebar-button-open');
	const closeSidebarButton = document.getElementById('sidebar-button-close');
	const closeOverlay = document.getElementById('sidebar-close-overlay');

	const openSidebar = () => {
		sidebar.classList.add('visible');
		openSidebarButton.blur();
	};

	const closeSidebar = () => {
		sidebar.classList.remove('visible');
		closeSidebarButton.blur();
	};

	openSidebarButton.addEventListener('click', () => openSidebar());
	closeSidebarButton.addEventListener('click', () => closeSidebar());
	closeOverlay.addEventListener('click', () => closeSidebar());


	let horizontalSwipeOrigin = 0;
	let horizontalSwipeDelta = 0;
	let verticalSwipeOrigin = 0;
	let verticalSwipeDelta = 0;
	let isScrolling = false;
	const DELTA_THRESHOLD = 100;

	document.body.addEventListener('touchstart', e => {
		if (e.touches.length > 1)
			return;
		isScrolling = false;
		horizontalSwipeOrigin = e.touches[0].clientX;
		verticalSwipeOrigin = e.touches[0].clientY;
	});

	document.body.addEventListener('touchmove', e => {
		if (isScrolling)
			return;

		horizontalSwipeDelta = e.touches[0].clientX - horizontalSwipeOrigin;
		verticalSwipeDelta = e.touches[0].clientY - verticalSwipeOrigin;

		if (Math.abs(verticalSwipeDelta) > DELTA_THRESHOLD)
			return;
		if (horizontalSwipeDelta > DELTA_THRESHOLD && !sidebar.classList.contains('visible'))
			openSidebar();
		else if (horizontalSwipeDelta < -DELTA_THRESHOLD && sidebar.classList.contains('visible'))
			closeSidebar();
	});

	window.addEventListener('scroll', () => {
		isScrolling = true;
	});

	Array.from(document.getElementsByTagName('pre')).forEach(pre => {
		pre.addEventListener('scroll', () => {
			isScrolling = true;
		});
	});
}
